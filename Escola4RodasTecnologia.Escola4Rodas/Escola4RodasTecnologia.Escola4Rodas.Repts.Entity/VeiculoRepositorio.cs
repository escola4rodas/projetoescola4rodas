﻿using Escola4RodasTecnologia.Escola4Rodas.Dominio;
using Escola4RodasTecnologia.Escola4Rodas.Entity.Context;
using Escola4RodasTecnologia.Repositorios.Comum.Entity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Escola4RodasTecnologia.Escola4Rodas.Repts.Entity
{
    public class VeiculoRepositorio : RepositorioGenericoEntity<Veiculo, int>
    {   
        public VeiculoRepositorio(Escola4RodasDbContext contexto)
            : base(contexto)
        {
            
        }

        public override List<Veiculo> Selecionar()
        {
            //_contexto.Set<TEntidade>().ToList();
            return _contexto.Set<Veiculo>()
                 .Include(v => v.Motorista)
                 .Include(v => v.Motorista.Usuario)
                 .Include(v => v.Documentos)
                 .Include(v => v.Viagens)
                 .ToList();
        }
    }
}
