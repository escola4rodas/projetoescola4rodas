﻿using Escola4RodasTecnologia.Escola4Rodas.Dominio;
using Escola4RodasTecnologia.Escola4Rodas.Entity.Context;
using Escola4RodasTecnologia.Repositorios.Comum.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace Escola4RodasTecnologia.Escola4Rodas.Repts.Entity
{
   public class MotoristaRepositorio : RepositorioGenericoEntity<Motorista, int>
    {
        public MotoristaRepositorio(Escola4RodasDbContext contexto)
            :base(contexto)
        {

        }

        public override List<Motorista> Selecionar()
        {   
            return _contexto.Set<Motorista>()
                 .Include(m => m.Instituicoes)
                 .Include(m => m.Passageiros)
                 .Include(m => m.Usuario)
                 .Include(m => m.Veiculos)
                 .Include(m => m.Viagens)
                 .ToList();
        }

        public override Motorista SelecionarPorId(int id)
        {
            return _contexto.Set<Motorista>()
                .Include(m => m.Instituicoes)
                .Include(m => m.Passageiros)
                .Include(m => m.Usuario)
                .Include(m => m.Veiculos)
                .Include(m => m.Viagens)
                .Where(m => m.Codigo_Usuario == id)
                .FirstOrDefault();
        }

        public override void Inserir(Motorista entidade)
        {
            _contexto.Set<Motorista>().Add(entidade);
            _contexto.SaveChanges();
        }
    }
}
