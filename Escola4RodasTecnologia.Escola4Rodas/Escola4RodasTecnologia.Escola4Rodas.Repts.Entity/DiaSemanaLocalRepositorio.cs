﻿using Escola4RodasTecnologia.Escola4Rodas.Dominio;
using Escola4RodasTecnologia.Escola4Rodas.Entity.Context;
using Escola4RodasTecnologia.Repositorios.Comum.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace Escola4RodasTecnologia.Escola4Rodas.Repts.Entity
{
    public class DiaSemanaLocalRepositorio : RepositorioGenericoEntity<DiaSemanaLocal, int>
    {
        public DiaSemanaLocalRepositorio(Escola4RodasDbContext contexto)
            :base(contexto)
        {

        }

        public override List<DiaSemanaLocal> Selecionar()
        {
            return _contexto.Set<DiaSemanaLocal>()
                .Include(m => m.DiaSemana)
                .Include(m => m.DiaSemanaLocais)
                .ToList();
        }
    }
}
