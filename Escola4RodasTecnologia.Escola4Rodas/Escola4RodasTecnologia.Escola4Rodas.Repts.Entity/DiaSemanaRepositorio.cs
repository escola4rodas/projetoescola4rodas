﻿using Escola4RodasTecnologia.Escola4Rodas.Dominio;
using Escola4RodasTecnologia.Escola4Rodas.Entity.Context;
using Escola4RodasTecnologia.Repositorios.Comum.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace Escola4RodasTecnologia.Escola4Rodas.Repts.Entity
{
    public class DiaSemanaRepositorio : RepositorioGenericoEntity<DiaSemana, int>
    {
        public DiaSemanaRepositorio(Escola4RodasDbContext contexto)
            :base(contexto)
        {

        }

        public override List<DiaSemana> Selecionar()
        {
            return _contexto.Set<DiaSemana>()
                .Include(v => v.DiaSemanaLocal)
                .Include(v => v.DiaSemanaViagem)
                .ToList();
        }
    }
}
