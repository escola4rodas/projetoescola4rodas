﻿using Escola4RodasTecnologia.Escola4Rodas.Dominio;
using Escola4RodasTecnologia.Escola4Rodas.Entity.Context;
using Escola4RodasTecnologia.Repositorios.Comum.Entity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Escola4RodasTecnologia.Escola4Rodas.Repts.Entity
{
    public class PagamentoRepositorio : RepositorioGenericoEntity<Pagamento, int>
    {
        public PagamentoRepositorio(Escola4RodasDbContext contexto)
            :base(contexto)
        {

        }

        public override List<Pagamento> Selecionar()
        {
            return _contexto.Set<Pagamento>()
                .Include(m => m.Passageiro)
                .Include(m => m.Passageiro.Usuario)
                .ToList();
        }
    }
}
