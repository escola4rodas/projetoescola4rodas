﻿using Escola4RodasTecnologia.Escola4Rodas.Dominio;
using Escola4RodasTecnologia.Escola4Rodas.Entity.Context;
using Escola4RodasTecnologia.Repositorios.Comum.Entity;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Escola4RodasTecnologia.Escola4Rodas.Repts.Entity
{
    public class PassageiroInstituicaoRepositorio : RepositorioGenericoEntity<PassageiroInstituicao, int>
    {
        public PassageiroInstituicaoRepositorio(Escola4RodasDbContext contexto)
            :base(contexto)
        {

        }

        public override List<PassageiroInstituicao> Selecionar()
        {
            return _contexto.Set<PassageiroInstituicao>()
                 .Include(m => m.Instituicao)
                 .Include(m => m.Passageiro)
                 .ToList();
        }
    }
}
