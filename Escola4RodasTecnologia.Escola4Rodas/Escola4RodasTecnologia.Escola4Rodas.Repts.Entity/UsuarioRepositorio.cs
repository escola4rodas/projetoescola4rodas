﻿using Escola4RodasTecnologia.Escola4Rodas.Dominio;
using Escola4RodasTecnologia.Escola4Rodas.Entity.Context;
using Escola4RodasTecnologia.Repositorios.Comum.Entity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Escola4RodasTecnologia.Escola4Rodas.Repts.Entity
{
    public class UsuarioRepositorio : RepositorioGenericoEntity<Usuario, int>
    {
        public UsuarioRepositorio(Escola4RodasDbContext contexto)
            :base(contexto)
        {

        }

        public override List<Usuario> Selecionar()
        {
            return _contexto.Set<Usuario>()
                 .Include(m => m.Motorista)
                 .Include(m => m.Passageiro)
                 .ToList();
        }
    }
}
