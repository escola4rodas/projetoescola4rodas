﻿using Escola4RodasTecnologia.Escola4Rodas.Dominio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Escola4RodasTecnologia.Escola4Rodas.Entity.Context;
using Escola4RodasTecnologia.Repositorios.Comum.Entity;

namespace Escola4RodasTecnologia.Escola4Rodas.Repts.Entity
{
    public class LocalInstituicaoRepositorio : RepositorioGenericoEntity<LocalInstituicao, int>
    {
        public LocalInstituicaoRepositorio(Escola4RodasDbContext contexto)
            :base(contexto)
        {

        }

        public override List<LocalInstituicao> Selecionar()
        {
            return _contexto.Set<LocalInstituicao>()
                 .Include(m => m.Instituicao)
                 .Include(m => m.Local)
                 .ToList();
        }
    }
}
