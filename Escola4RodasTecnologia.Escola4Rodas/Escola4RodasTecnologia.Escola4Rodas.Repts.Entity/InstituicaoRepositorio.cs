﻿using Escola4RodasTecnologia.Escola4Rodas.Dominio;
using Escola4RodasTecnologia.Escola4Rodas.Entity.Context;
using Escola4RodasTecnologia.Repositorios.Comum.Entity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Escola4RodasTecnologia.Escola4Rodas.Repts.Entity
{
    public class InstituicaoRepositorio : RepositorioGenericoEntity<Instituicao, int>
    {
        public InstituicaoRepositorio(Escola4RodasDbContext contexto)
            :base(contexto)
        {

        }

        public override Instituicao SelecionarPorId(int id)
        {
            return _contexto.Set<Instituicao>()
                //.Include(v => v.LocalInstituicao)
                //.Include(v => v.LocalInstituicao.Instituicao)
                .Include(v => v.LocalInstituicao)
                .Include(v => v.Motoristas)
                .Include(v => v.PassageiroInstituicao)
                .Include(v => v.Viagens)
                .FirstOrDefault();
        }
    }
}
