﻿using Escola4RodasTecnologia.Escola4Rodas.Entity.Context;
using Escola4RodasTecnologia.Repositorios.Comum.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Threading.Tasks;
using Escola4RodasTecnologia.Escola4Rodas.Dominio;

namespace Escola4RodasTecnologia.Escola4Rodas.Repts.Entity
{
    public class LocalPassageiroRepositorio : RepositorioGenericoEntity<LocalPassageiro, int>
    {
        public LocalPassageiroRepositorio(Escola4RodasDbContext contexto)
            :base(contexto)
        {

        }

        public override List<LocalPassageiro> Selecionar()
        {
            return _contexto.Set<LocalPassageiro>()
                 .Include(m => m.DiasSemanaLocal)
                 .Include(m => m.Local)
                 .Include(m => m.Passageiro)
                 .ToList();
        }
    }
}
