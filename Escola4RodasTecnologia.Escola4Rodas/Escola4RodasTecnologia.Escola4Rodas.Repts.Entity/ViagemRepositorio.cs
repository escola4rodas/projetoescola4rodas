﻿using Escola4RodasTecnologia.Escola4Rodas.Dominio;
using Escola4RodasTecnologia.Escola4Rodas.Entity.Context;
using Escola4RodasTecnologia.Repositorios.Comum.Entity;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Escola4RodasTecnologia.Escola4Rodas.Repts.Entity
{
    public class ViagemRepositorio : RepositorioGenericoEntity<Viagem, int>
    {
        Escola4RodasDbContext _contextoViagem;

        private InstituicaoRepositorio _repositorioInstituicao
                = new InstituicaoRepositorio(new Escola4RodasDbContext());


        public ViagemRepositorio(Escola4RodasDbContext contexto)
            :base(contexto)
        {
            _contextoViagem = contexto;
        }
        public override List<Viagem> Selecionar()
        {
            return _contexto.Set<Viagem>()
                .Include(v => v.Instituicoes)
                .Include(v => v.Passageiros)
                .Include(v => v.VeiculoViagem)
                .Include(v => v.VeiculoViagem.Motorista)
                .Include(v => v.VeiculoViagem.Motorista.Usuario)
                .Include(v => v.DiaSemanaViagem)
                
                .ToList();
        }

        public override Viagem SelecionarPorId(int id)
        {
            return _contexto.Set<Viagem>()
                .Include(m => m.DiaSemanaViagem)
                .Include(m => m.Instituicoes)
                .Include(m => m.Passageiros)
                .Include(m => m.VeiculoViagem)
                .ToList()
                .Where(m => m.Codigo == id)
                .FirstOrDefault();
        }
        public override void Alterar(Viagem entidade)
        {
            Viagem viagemBanco = _contextoViagem.Viagem
                .Include(m => m.Instituicoes)
                .Include(m => m.Passageiros)
                .Include(m => m.VeiculoViagem)
                .Include(m => m.DiaSemanaViagem)
                .FirstOrDefault(v => v.Codigo ==  entidade.Codigo);

            _contextoViagem.Set<Viagem>().Attach(viagemBanco);
            
            viagemBanco.Instituicoes.Clear();

            Instituicao registro;
            foreach (var item in entidade.Instituicoes)
            {
                registro = _contextoViagem.Instituicao.FirstOrDefault(i => i.Codigo_Instituicao == item.Codigo_Instituicao);
                if(viagemBanco.Instituicoes
                    .Where(v => v.Codigo_Instituicao == registro.Codigo_Instituicao)
                    .ToList()
                    .Count == 0)
                {
                    viagemBanco.Instituicoes.Add(registro);
                }
            }

            viagemBanco.DiaSemanaViagem.Clear();
            Dominio.DiaSemanaViagem diaSemanaViagemBanco;
            foreach (var item in entidade.DiaSemanaViagem)
            {
                diaSemanaViagemBanco = _contextoViagem.DiaSemanaViagem.FirstOrDefault(d => d.Codigo == item.Codigo);
                viagemBanco.DiaSemanaViagem.Add(diaSemanaViagemBanco);
            }

            viagemBanco.Passageiros.Clear();
            Dominio.Passageiro passageiroViagemBanco;
            foreach (var item in entidade.Passageiros)
            {
                passageiroViagemBanco = _contextoViagem.Passageiro.FirstOrDefault(p => p.Codigo_Usuario == item.Codigo_Usuario);
                viagemBanco.Passageiros.Add(passageiroViagemBanco);
            }

            viagemBanco.VeiculoViagem = _contextoViagem.Veiculo.FirstOrDefault(v => v.Codigo_Veiculo == entidade.VeiculoViagem.Codigo_Veiculo);


            _contextoViagem.Entry(viagemBanco).Entity.Instituicoes = viagemBanco.Instituicoes;
            //_contextoViagem.Entry(viagemBanco).Entity.Passageiros = viagemBanco.Passageiros;
            _contextoViagem.Entry(viagemBanco).Entity.VeiculoViagem = viagemBanco.VeiculoViagem;
            _contextoViagem.Entry(viagemBanco).Entity.DiaSemanaViagem = viagemBanco.DiaSemanaViagem;
            _contextoViagem.Entry(viagemBanco).Entity.Codigo_Veiculo = viagemBanco.VeiculoViagem.Codigo_Veiculo;
            _contextoViagem.Entry(viagemBanco).Entity.Tipo_Viagem = viagemBanco.Tipo_Viagem;
            _contextoViagem.Entry(viagemBanco).Entity.Data_Inicio = viagemBanco.Data_Inicio;

            _contextoViagem.Entry(viagemBanco).State = EntityState.Modified;
            
            _contextoViagem.SaveChanges();
        }
    }
}
