﻿using Escola4RodasTecnologia.Escola4Rodas.Dominio;
using Escola4RodasTecnologia.Escola4Rodas.Entity.Context;
using Escola4RodasTecnologia.Repositorios.Comum.Entity;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Escola4RodasTecnologia.Escola4Rodas.Repts.Entity
{
    public class DiaSemanaViagemRepositorio : RepositorioGenericoEntity<DiaSemanaViagem, int>
    {
        public DiaSemanaViagemRepositorio(Escola4RodasDbContext contexto)
            :base(contexto)
        {
            var teste = 123;
        }

        public override List<DiaSemanaViagem> Selecionar()
        {
            return _contexto.Set<DiaSemanaViagem>()
                 .Include(m => m.DiaSemana)
                 .Include(m => m.ViagemDiaSemana)
                 .ToList();
        }
    }
}
