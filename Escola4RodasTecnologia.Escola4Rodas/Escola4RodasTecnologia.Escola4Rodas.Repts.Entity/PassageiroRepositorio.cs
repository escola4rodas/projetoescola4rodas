﻿using Escola4RodasTecnologia.Escola4Rodas.Dominio;
using Escola4RodasTecnologia.Escola4Rodas.Entity.Context;
using Escola4RodasTecnologia.Repositorios.Comum.Entity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Escola4RodasTecnologia.Escola4Rodas.Repts.Entity
{
    public class PassageiroRepositorio : RepositorioGenericoEntity<Passageiro, int>
    {
        Escola4RodasDbContext _contextoPassageiro;
       
        

        public PassageiroRepositorio(Escola4RodasDbContext contexto)
            :base(contexto)
        {
            this._contextoPassageiro = contexto;
        }

        public override List<Passageiro> Selecionar()
        {
            return _contexto.Set<Passageiro>()
                .Include(m => m.Usuario)
                .Include(m => m.Motorista)
                .Include(m => m.LocaisPassageiro)
                .Include(m => m.Pagamentos)
                .Include(m => m.PassageiroInstituicao)
                .Include(m => m.Pagamentos)
                .Include(m => m.Viagens)
                .ToList();
        }

        public override void Alterar(Passageiro entidade)
        {
            Passageiro passageiroBanco = _contextoPassageiro.Passageiro
                .Include(m => m.Motorista)
                .Include(m => m.Pagamentos)
                .Include(m => m.LocaisPassageiro)
                .Include(m => m.PassageiroInstituicao)
                .Include(m => m.Viagens)
                .FirstOrDefault(v => v.Codigo_Usuario == entidade.Codigo_Usuario);
            
            _contextoPassageiro.Set<Passageiro>().Attach(passageiroBanco);

            passageiroBanco.LocaisPassageiro.Clear();
            LocalPassageiro registroLocalPassageiro;
            foreach (var item in entidade.LocaisPassageiro)     
            {
                registroLocalPassageiro = _contextoPassageiro.LocalPassageiro.FirstOrDefault(d => d.Codigo_Local == item.Local.Codigo);
                passageiroBanco.LocaisPassageiro.Add(registroLocalPassageiro);
            }
            
            passageiroBanco.PassageiroInstituicao.Clear();
            PassageiroInstituicao registroPassageiroInstituicao;
            foreach (var item in entidade.PassageiroInstituicao)
            {
                registroPassageiroInstituicao = _contextoPassageiro.PassageiroInstituicao
                    .FirstOrDefault(p => p.Codigo_Instituicao == item.Codigo_Instituicao && p.Codigo_Passageiro == item.Codigo_Passageiro);

                if (registroPassageiroInstituicao == null)
                {
                    registroPassageiroInstituicao = new PassageiroInstituicao
                    {
                        Codigo_Instituicao = item.Codigo_Instituicao,
                        Codigo_Passageiro = item.Codigo_Passageiro,
                        Codigo_Tipo_Passageiro = item.Codigo_Tipo_Passageiro,
                        Instituicao = _contextoPassageiro.Instituicao
                            .FirstOrDefault(p => p.Codigo_Instituicao == item.Codigo_Instituicao),
                        Passageiro = _contextoPassageiro.Passageiro
                            .FirstOrDefault(p => p.Codigo_Usuario == item.Codigo_Passageiro)
                    };
                }
                else
                {
                    registroPassageiroInstituicao.Instituicao = _contextoPassageiro.Instituicao
                            .FirstOrDefault(p => p.Codigo_Instituicao == item.Codigo_Instituicao);
                }
                
                passageiroBanco.PassageiroInstituicao.Add(registroPassageiroInstituicao);
            }

            Passageiro passageiroPesquisaViagem = _contextoPassageiro.Passageiro
                .FirstOrDefault(p => p.Codigo_Usuario == entidade.Codigo_Usuario);
            List<Viagem> viagens = _contextoPassageiro.Viagem
                .Where(v => v.VeiculoViagem.Codigo_Motorista == entidade.Motorista.Codigo_Usuario)
                .ToList();
            passageiroBanco.Viagens.Clear();
            foreach (var item in viagens)
            {
                foreach (var item2 in item.Passageiros)
                {
                    if(item2.Codigo_Usuario == entidade.Codigo_Usuario)
                    {
                        passageiroBanco.Viagens.Add(item);
                        break;
                    }
                }
            }

            List<Pagamento> pagamentos = _contextoPassageiro.Pagamento
                .Where(v => v.Codigo_Passageiro == entidade.Codigo_Usuario)
                .ToList();
            passageiroBanco.Pagamentos.Clear();
            foreach (var item in pagamentos)
            {
                passageiroBanco.Pagamentos.Add(item);
            }

            passageiroBanco.Usuario = _contextoPassageiro.Usuario.FirstOrDefault(u => u.Codigo == entidade.Codigo_Usuario);
            passageiroBanco.Motorista = _contextoPassageiro.Motorista.FirstOrDefault(v => v.Codigo_Usuario == entidade.Motorista.Codigo_Usuario);

            passageiroBanco.Usuario.Nome = entidade.Usuario.Nome;
            passageiroBanco.Usuario.Idade = entidade.Usuario.Idade;
            passageiroBanco.Usuario.Telefone = entidade.Usuario.Telefone;
            passageiroBanco.Usuario.Email = entidade.Usuario.Email;

            _contextoPassageiro.Entry(passageiroBanco).Entity.Usuario = passageiroBanco.Usuario;
            _contextoPassageiro.Entry(passageiroBanco).Entity.Usuario = passageiroBanco.Usuario;
            _contextoPassageiro.Entry(passageiroBanco).Entity.Motorista = passageiroBanco.Motorista;
            _contextoPassageiro.Entry(passageiroBanco).Entity.PassageiroInstituicao = passageiroBanco.PassageiroInstituicao;
            _contextoPassageiro.Entry(passageiroBanco).Entity.LocaisPassageiro= passageiroBanco.LocaisPassageiro;
            _contextoPassageiro.Entry(passageiroBanco).Entity.Viagens = passageiroBanco.Viagens;
            _contextoPassageiro.Entry(passageiroBanco).Entity.Pagamentos = passageiroBanco.Pagamentos;

            _contextoPassageiro.Entry(passageiroBanco).State = EntityState.Modified;

            _contextoPassageiro.SaveChanges();
        }
    }
}
