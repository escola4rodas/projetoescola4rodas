﻿using System.Collections.Generic;
using Escola4RodasTecnologia.Escola4Rodas.Dominio;
using Escola4RodasTecnologia.Escola4Rodas.Entity.Context;
using Escola4RodasTecnologia.Repositorios.Comum.Entity;
using System.Data.Entity;
using System.Linq;

namespace Escola4RodasTecnologia.Escola4Rodas.Repts.Entity
{
    public class RotaRepositorio : RepositorioGenericoEntity<Rota, int>
    {
        public RotaRepositorio(Escola4RodasDbContext contexto)
            :base(contexto)
        {
            
        }

        public override List<Rota> Selecionar()
        {
            return _contexto.Set<Rota>()
                 .Include(m => m.Passageiros)
                 .ToList();
        }
    }
}
