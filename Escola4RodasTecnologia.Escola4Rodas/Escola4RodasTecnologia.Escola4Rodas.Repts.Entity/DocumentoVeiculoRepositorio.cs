﻿using Escola4RodasTecnologia.Escola4Rodas.Dominio;
using Escola4RodasTecnologia.Escola4Rodas.Entity.Context;
using Escola4RodasTecnologia.Repositorios.Comum.Entity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Escola4RodasTecnologia.Escola4Rodas.Repts.Entity
{
    public class DocumentoVeiculoRepositorio : RepositorioGenericoEntity<DocumentoVeiculo, int>
    {
        public DocumentoVeiculoRepositorio(Escola4RodasDbContext contexto)
            :base(contexto)
        {

        }

        public override List<DocumentoVeiculo> Selecionar()
        {
            return _contexto.Set<DocumentoVeiculo>()
                 .Include(m => m.Veiculo)
                 .ToList();
        }
    }
}
