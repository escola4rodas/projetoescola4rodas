﻿using Escola4RodasTecnologia.Escola4Rodas.Dominio;
using Escola4RodasTecnologia.Escola4Rodas.Entity.Context;
using Escola4RodasTecnologia.Repositorios.Comum.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Escola4RodasTecnologia.Escola4Rodas.Repts.Entity
{
    public class ViagemDiaSemanaRepositorio : RepositorioGenericoEntity<ViagemDiaSemana, int>
    {
        public ViagemDiaSemanaRepositorio(Escola4RodasDbContext contexto)
            :base(contexto)
        {

        }
    
    }
}
