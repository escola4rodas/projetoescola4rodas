﻿using Escola4RodasTecnologia.Comum.Entity;
using Escola4RodasTecnologia.Escola4Rodas.Dominio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Escola4RodasTecnologia.Escola4Rodas.Entity.TypeConfiguration
{
    public class UsuarioTypeConfiguration : Escola4RodasEntityAbstractConfig<Usuario>
    {
        protected override void ConfigurarCamposTabela()
        {
            Property(p => p.Codigo)
                .IsRequired()
                .HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity)
                .HasColumnName("codigo");

            Property(p => p.Nome)
                .IsRequired()
                .HasMaxLength(100)
                .HasColumnName("nome");

            Property(p => p.Idade)
                .IsRequired()
                .HasColumnName("idade");

            Property(p => p.Telefone)
                .IsRequired()
                .HasColumnName("telefone")
                .HasMaxLength(22);

            Property(p => p.Email)
                .IsRequired()
                .HasColumnName("email")
                .HasMaxLength(50);

            Property(p => p.Login)
                .IsRequired()
                .HasColumnName("login")
                .HasMaxLength(20);

            Property(p => p.Senha)
                .IsRequired()
                .HasColumnName("senha");
        }

        protected override void ConfigurarChaveEstrangeiraTabela()
        {
            HasOptional(p => p.Passageiro)
                .WithRequired(p => p.Usuario);

            HasOptional(p => p.Motorista)
                .WithRequired(p => p.Usuario);
                
        }

        protected override void ConfigurarChavePrimariaTabela()
        {
            HasKey(p => p.Codigo);
        }

        protected override void ConfigurarNomeTabela()
        {
            ToTable("Usuario");
        }
    }
}
