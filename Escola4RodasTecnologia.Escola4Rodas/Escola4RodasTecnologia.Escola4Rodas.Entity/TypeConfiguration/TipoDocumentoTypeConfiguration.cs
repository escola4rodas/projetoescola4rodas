﻿using Escola4RodasTecnologia.Comum.Entity;
using Escola4RodasTecnologia.Escola4Rodas.Dominio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Escola4RodasTecnologia.Escola4Rodas.Entity.TypeConfiguration
{
    public class TipoDocumentoTypeConfiguration : Escola4RodasEntityAbstractConfig<TipoDocumento>
    {
        protected override void ConfigurarCamposTabela()
        {
            Property(p => p.Codigo)
                .HasColumnName("codigo")
                .IsRequired()
                .HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);

            Property(p => p.Descricao)
                .HasColumnName("descricao")
                .IsRequired();

            Property(p => p.Dthr)
                .IsRequired()
                .HasColumnName("dthr");
        }

        protected override void ConfigurarChaveEstrangeiraTabela()
        {
            
        }

        protected override void ConfigurarChavePrimariaTabela()
        {
            HasKey(p => p.Codigo);
        }

        protected override void ConfigurarNomeTabela()
        {
			//Permite que o motorista crie seus próprios documentos
            ToTable("Tipo_documento_dinamico_motorista");
        }
    }
}
