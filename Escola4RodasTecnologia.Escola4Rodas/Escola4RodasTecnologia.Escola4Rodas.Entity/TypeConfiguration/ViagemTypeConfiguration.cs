﻿using Escola4RodasTecnologia.Comum.Entity;
using Escola4RodasTecnologia.Escola4Rodas.Dominio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Escola4RodasTecnologia.Escola4Rodas.Entity.TypeConfiguration
{
    public class ViagemTypeConfiguration : Escola4RodasEntityAbstractConfig<Viagem>
    {
        protected override void ConfigurarCamposTabela()
        {
            Property(p => p.Codigo)
                .IsRequired()
                .HasColumnName("codigo")
                .HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);

            Property(p => p.Nome)
                .IsRequired()
                .HasColumnName("nome");

            Property(p => p.Data_Inicio)
                .IsRequired()
                .HasColumnName("data_inicio");
            
            Property(p => p.Codigo_Veiculo)
                .IsRequired()
                .HasColumnName("codigo_veiculo");

            Property(p => p.Codigo_Dia_Semana)
                .IsRequired()
                .HasColumnName("codigo_dia_semana");

            Property(p => p.Dthr)
                .IsRequired()
                .HasColumnName("dthr");
        }

        protected override void ConfigurarChaveEstrangeiraTabela()
        {
            HasMany(p => p.Instituicoes)
                .WithMany(p => p.Viagens)
                .Map(iv =>
                {
                    iv.MapLeftKey("codigo_viagem");
                    iv.MapRightKey("codigo_instituicao");
                    iv.ToTable("viagem_instituicao");
                });

            HasMany(p => p.DiaSemanaViagem)
                .WithMany(p => p.ViagemDiaSemana)
                .Map(iv =>
                {
                    iv.MapLeftKey("codigo_viagem");
                    iv.MapRightKey("codigo_dia_semana");
                    iv.ToTable("viagem_dia_semana");
                });
        }

        protected override void ConfigurarChavePrimariaTabela()
        {
            HasKey(p => p.Codigo);
        }

        protected override void ConfigurarNomeTabela()
        {
            ToTable("viagem");
        }
    }
}
