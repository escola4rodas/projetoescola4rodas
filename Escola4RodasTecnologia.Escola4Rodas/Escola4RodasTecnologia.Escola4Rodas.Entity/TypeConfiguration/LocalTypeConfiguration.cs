﻿using Escola4RodasTecnologia.Comum.Entity;
using Escola4RodasTecnologia.Escola4Rodas.Dominio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Escola4RodasTecnologia.Escola4Rodas.Entity.TypeConfiguration
{
    public class LocalTypeConfiguration : Escola4RodasEntityAbstractConfig<Local>
    {
        protected override void ConfigurarCamposTabela()
        {
            Property(p => p.Codigo)
                .IsRequired()
                .HasColumnName("codigo")
                .HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);

            Property(p => p.Latitude)
                .IsRequired()
                .HasColumnName("latitude");

            Property(p => p.Longitude)
                .IsRequired()
                .HasColumnName("longitude");

            Property(p => p.Nome_Local)
                .IsRequired()
                .HasColumnName("nome_local");

            Property(p => p.Bairro)
                .IsRequired()
                .HasColumnName("bairro")
                .HasMaxLength(50);

            Property(p => p.Nome_Rua)
                .IsRequired()
                .HasColumnName("rua")
                .HasMaxLength(50);

            Property(p => p.Numero)
                .IsRequired()
                .HasColumnName("numero");
            
        }

        protected override void ConfigurarChaveEstrangeiraTabela()
        {
            HasOptional(p => p.LocalPassageiro)
                .WithRequired(p => p.Local);

            HasOptional(p => p.LocalInstituicao)
                .WithRequired(p => p.Local);
        }

        protected override void ConfigurarChavePrimariaTabela()
        {
            HasKey(p => p.Codigo);
        }

        protected override void ConfigurarNomeTabela()
        {
            ToTable("local");
        }
    }
}
