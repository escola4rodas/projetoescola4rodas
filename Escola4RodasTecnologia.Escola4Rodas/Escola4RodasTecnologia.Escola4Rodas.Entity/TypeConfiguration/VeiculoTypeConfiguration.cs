﻿using Escola4RodasTecnologia.Comum.Entity;
using Escola4RodasTecnologia.Escola4Rodas.Dominio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Escola4RodasTecnologia.Escola4Rodas.Entity.TypeConfiguration
{
    public class VeiculoTypeConfiguration : Escola4RodasEntityAbstractConfig<Veiculo>
    {
        protected override void ConfigurarCamposTabela()
        {
            Property(p => p.Codigo_Veiculo)
                .IsRequired()
                .HasColumnName("codigo_veiculo")
                .HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);

            Property(p => p.Nome)
                .IsRequired()
                .HasColumnName("nome");

            Property(p => p.Placa)
                .IsRequired()
                .HasColumnName("placa");

            Property(p => p.Carga_Maxima)
                .IsRequired()
                .HasColumnName("carga_maxima");

            Property(p => p.Codigo_Motorista)
                .IsRequired()
                .HasColumnName("codigo_motorista");

            Property(p => p.Dthr)
                .IsRequired()
                .HasColumnName("dthr");
        }

        protected override void ConfigurarChaveEstrangeiraTabela()
        {
            HasMany(p => p.Documentos)
                .WithRequired(p => p.Veiculo)
                .HasForeignKey(p => p.Codigo_Veiculo);

            HasMany(p => p.Viagens)
                .WithRequired(p => p.VeiculoViagem)
                .HasForeignKey(p => p.Codigo_Veiculo);

        }

        protected override void ConfigurarChavePrimariaTabela()
        {
            HasKey(pk => pk.Codigo_Veiculo);
        }

        protected override void ConfigurarNomeTabela()
        {
            ToTable("veiculo");
        }
    }
}
