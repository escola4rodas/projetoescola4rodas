﻿using Escola4RodasTecnologia.Comum.Entity;
using Escola4RodasTecnologia.Escola4Rodas.Dominio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Escola4RodasTecnologia.Escola4Rodas.Entity.TypeConfiguration
{
    public class DocumentoVeiculoTypeConfiguration : Escola4RodasEntityAbstractConfig<DocumentoVeiculo>
    {
        protected override void ConfigurarCamposTabela()
        {
            Property(p => p.Codigo)
                .IsRequired()
                .HasColumnName("codigo")
                .HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);

            Property(p => p.Descricao)
                .HasMaxLength(100)
                .HasColumnName("descricao");

            Property(p => p.Validade)
                .IsRequired()
                .HasColumnName("validade");

            Property(p => p.Codigo_Veiculo)
                .IsRequired()
                .HasColumnName("codigo_veiculo");

            Property(p => p.Nome_Documento)
                .IsRequired()
                .HasColumnName("nome_documento");
				
			Property(p => p.Codigo_Tipo_Documento)
				.IsRequired()
				.HasColumnName("tipo_documento");
            
        }
		
        protected override void ConfigurarChavePrimariaTabela()
        {
            HasKey(p => p.Codigo);
        }

		protected override void ConfigurarChaveEstrangeiraTabela()
        {
            //HasRequired(p => p.TipoDocumento)
            //    .WithRequiredPrincipal(p => p.Local);
        }
		
        protected override void ConfigurarNomeTabela()
        {
            ToTable("documento_veiculo");
        }
    }
}
