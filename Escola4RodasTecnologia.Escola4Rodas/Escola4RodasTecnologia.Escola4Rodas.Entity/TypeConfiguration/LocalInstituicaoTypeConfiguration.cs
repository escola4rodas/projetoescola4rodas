﻿using Escola4RodasTecnologia.Comum.Entity;
using Escola4RodasTecnologia.Escola4Rodas.Dominio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Escola4RodasTecnologia.Escola4Rodas.Entity.TypeConfiguration
{
    public class LocalInstituicaoTypeConfiguration : Escola4RodasEntityAbstractConfig<LocalInstituicao>
    {
        protected override void ConfigurarCamposTabela()
        {
            
            Property(p => p.Codigo_Local)
                .IsRequired()
                .HasColumnName("codigo_local");            
            
        }

        protected override void ConfigurarChaveEstrangeiraTabela()
        {
            
        }

        protected override void ConfigurarChavePrimariaTabela()
        {
            HasKey(p => p.Codigo_Local);
        }

        protected override void ConfigurarNomeTabela()
        {
            ToTable("Local_Instituicao");
        }
    }
}
