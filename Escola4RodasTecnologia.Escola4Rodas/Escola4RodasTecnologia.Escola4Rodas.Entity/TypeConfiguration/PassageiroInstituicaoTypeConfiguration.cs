﻿using Escola4RodasTecnologia.Comum.Entity;
using Escola4RodasTecnologia.Escola4Rodas.Dominio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Escola4RodasTecnologia.Escola4Rodas.Entity.TypeConfiguration
{
    public class PassageiroInstituicaoTypeConfiguration : Escola4RodasEntityAbstractConfig<PassageiroInstituicao>
    {
        protected override void ConfigurarCamposTabela()
        {
            Property(pi => pi.Codigo_Passageiro)
                .IsRequired()
                .HasColumnName("codigo_passageiro");

            Property(pi => pi.Codigo_Instituicao)
                .IsRequired()
                .HasColumnName("codigo_instituicao");

            Property(pi => pi.Codigo_Tipo_Passageiro)
                .IsRequired()
                .HasColumnName("codigo_tipo_passageiro");
        }

        protected override void ConfigurarChaveEstrangeiraTabela()
        {   
            
        }

        protected override void ConfigurarChavePrimariaTabela()
        {
            HasKey(pi => new { pi.Codigo_Passageiro, pi.Codigo_Instituicao });
        }

        protected override void ConfigurarNomeTabela()
        {
            ToTable("Passageiro_Instituicao");
        }
    }
}
