﻿using Escola4RodasTecnologia.Comum.Entity;
using Escola4RodasTecnologia.Escola4Rodas.Dominio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Escola4RodasTecnologia.Escola4Rodas.Entity.TypeConfiguration
{
    public class TipoLocalTypeConfiguration : Escola4RodasEntityAbstractConfig<TipoLocal>
    {
        protected override void ConfigurarCamposTabela()
        {
            Property(p => p.Codigo_Tipo_Local)
                .IsRequired()
                .HasColumnName("codigo_tipo_local")
                .HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);

            Property(p => p.Descricao)
                .IsRequired()
                .HasColumnName("descricao")
                .HasMaxLength(50);
        }

        protected override void ConfigurarChaveEstrangeiraTabela()
        {
            
        }

        protected override void ConfigurarChavePrimariaTabela()
        {
            HasKey(p => p.Codigo_Tipo_Local);
        }

        protected override void ConfigurarNomeTabela()
        {
            ToTable("tipo_local");
        }
    }
}
