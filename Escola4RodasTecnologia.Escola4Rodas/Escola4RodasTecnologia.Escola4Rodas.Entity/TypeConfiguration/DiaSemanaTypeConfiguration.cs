﻿using Escola4RodasTecnologia.Comum.Entity;
using Escola4RodasTecnologia.Escola4Rodas.Dominio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Escola4RodasTecnologia.Escola4Rodas.Entity.TypeConfiguration
{
    public class DiaSemanaTypeConfiguration : Escola4RodasEntityAbstractConfig<DiaSemana>
    {
        protected override void ConfigurarCamposTabela()
        {
            Property(p => p.Codigo)
                .IsRequired()
                .HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity)
                .HasColumnName("codigo");

            Property(p => p.Dia_Semana)
                .IsRequired()
                .HasColumnName("dia_semana")
                .HasMaxLength(13);
        }

        protected override void ConfigurarChaveEstrangeiraTabela()
        {
            HasOptional(p => p.DiaSemanaLocal)
                .WithRequired(p => p.DiaSemana);

            HasOptional(p => p.DiaSemanaViagem)
                .WithRequired(p => p.DiaSemana);

        }

        protected override void ConfigurarChavePrimariaTabela()
        {
            HasKey(p => p.Codigo);
        }

        protected override void ConfigurarNomeTabela()
        {
            ToTable("Dia_Semana");
        }
    }
}
