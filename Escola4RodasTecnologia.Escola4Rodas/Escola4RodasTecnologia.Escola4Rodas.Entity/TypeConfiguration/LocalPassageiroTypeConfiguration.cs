﻿using Escola4RodasTecnologia.Comum.Entity;
using Escola4RodasTecnologia.Escola4Rodas.Dominio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Escola4RodasTecnologia.Escola4Rodas.Entity.TypeConfiguration
{
    public class LocalPassageiroTypeConfiguration : Escola4RodasEntityAbstractConfig<LocalPassageiro>
    {
        protected override void ConfigurarCamposTabela()
        {
            Property(p => p.Codigo_Local)
                .IsRequired()
                .HasColumnName("codigo_local");

            Property(p => p.Codigo_Tipo_Local)
                .IsRequired()
                .HasColumnName("codigo_tipo_local");

            Property(p => p.Codigo_Passageiro)
                .IsRequired()
                .HasColumnName("codigo_passageiro");
            
        }

        protected override void ConfigurarChaveEstrangeiraTabela()
        {
            HasMany(p => p.DiasSemanaLocal)
                .WithMany(p => p.DiaSemanaLocais)
                .Map(lds =>
                {
                    lds.MapLeftKey("codigo_local");
                    lds.MapRightKey("codigo_dia_semana");
                    lds.ToTable("localpassageiro_dia_semana");
                });
        }

        protected override void ConfigurarChavePrimariaTabela()
        {
            HasKey(p => p.Codigo_Local);
        }

        protected override void ConfigurarNomeTabela()
        {
            ToTable("Local_Passageiro");
        }
    }
}
