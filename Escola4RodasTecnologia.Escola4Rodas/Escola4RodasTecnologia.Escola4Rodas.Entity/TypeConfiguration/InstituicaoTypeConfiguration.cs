﻿using Escola4RodasTecnologia.Comum.Entity;
using Escola4RodasTecnologia.Escola4Rodas.Dominio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Escola4RodasTecnologia.Escola4Rodas.Entity.TypeConfiguration
{
    public class InstituicaoTypeConfiguration : Escola4RodasEntityAbstractConfig<Instituicao>
    {
        protected override void ConfigurarCamposTabela()
        {
            Property(p => p.Codigo_Instituicao)
                .IsRequired()
                .HasColumnName("codigo_instituicao")
                .HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);

            Property(p => p.Nome)
                .IsRequired()
                .HasColumnName("nome");

            //Property(p => p.Codigo_Local)
            //    .IsRequired()
            //    .HasColumnName("codigo_local");

            Property(p => p.Dthr)
                .IsRequired()
                .HasColumnName("dthr");
        }

        protected override void ConfigurarChaveEstrangeiraTabela()
        {
            HasMany(p => p.PassageiroInstituicao)
                .WithRequired(p => p.Instituicao)
                .HasForeignKey(p => p.Codigo_Instituicao);

            HasOptional(p => p.LocalInstituicao)
                .WithOptionalPrincipal(p => p.Instituicao);
        }

        protected override void ConfigurarChavePrimariaTabela()
        {
            HasKey(p => p.Codigo_Instituicao);
        }

        protected override void ConfigurarNomeTabela()
        {
            ToTable("instituicao");
        }
    }
}
