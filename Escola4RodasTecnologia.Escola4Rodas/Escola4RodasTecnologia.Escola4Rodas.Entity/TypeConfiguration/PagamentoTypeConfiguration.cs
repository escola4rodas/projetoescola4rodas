﻿using Escola4RodasTecnologia.Comum.Entity;
using Escola4RodasTecnologia.Escola4Rodas.Dominio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Escola4RodasTecnologia.Escola4Rodas.Entity.TypeConfiguration
{
    public class PagamentoTypeConfiguration : Escola4RodasEntityAbstractConfig<Pagamento>
    {
        protected override void ConfigurarCamposTabela()
        {
            Property(p => p.Codigo)
                .IsRequired()
                .HasColumnName("codigo");

            Property(p => p.Descricao)
                .IsOptional()
                .HasColumnName("descricao");

            Property(p => p.Dthr)
                .IsRequired()
                .HasColumnName("dthr");

            Property(p => p.Codigo_Passageiro)
                .IsRequired()
                .HasColumnName("codigo_passageiro");

            Property(p => p.Mes)
                .IsRequired()
                .HasColumnName("mes");

            Property(p => p.Ano)
                .IsRequired()
                .HasColumnName("ano");

            Property(p => p.Valor)
                .IsRequired()
                .HasColumnName("valor");
        }

        protected override void ConfigurarChaveEstrangeiraTabela()
        {
            
        }

        protected override void ConfigurarChavePrimariaTabela()
        {
            HasKey(p => p.Codigo);
        }

        protected override void ConfigurarNomeTabela()
        {
            ToTable("pagamento");
        }
    }
}
