﻿using Escola4RodasTecnologia.Comum.Entity;
using Escola4RodasTecnologia.Escola4Rodas.Dominio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Escola4RodasTecnologia.Escola4Rodas.Entity.TypeConfiguration
{
    public class MotoristaTypeConfiguration : Escola4RodasEntityAbstractConfig<Motorista>
    {
        protected override void ConfigurarCamposTabela()
        {
            Property(p => p.Codigo_Usuario)
                .HasColumnName("codigo_usuario")
                .IsRequired();
           
            Property(p => p.Cnh)
                .IsRequired()
                .HasColumnName("cnh");
            
        }

        protected override void ConfigurarChaveEstrangeiraTabela()
        {
            
            HasMany(p => p.Veiculos)
                .WithRequired(p => p.Motorista)
                .HasForeignKey(fk => fk.Codigo_Motorista);

            HasMany(p => p.Instituicoes)
                .WithMany(p => p.Motoristas)
                .Map(mins => {
                                mins.MapLeftKey("codigo_motorista");
                                mins.MapRightKey("codigo_instituicao");
                                mins.ToTable("motorista_instituicao");
                             });

            HasMany(p => p.Passageiros)
                .WithRequired(p => p.Motorista)
                .HasForeignKey(fk => fk.Codigo_Motorista);

            //HasMany(p => p.Locais_Partida)
            //    .WithRequired(p => p.Passageiro)
            //    .HasForeignKey(fk => fk.Codigo_Passageiro);
        }

        protected override void ConfigurarChavePrimariaTabela()
        {
            HasKey(p => new { p.Codigo_Usuario});
        }

        protected override void ConfigurarNomeTabela()
        {
            ToTable("Motorista");
        }
    }
}
