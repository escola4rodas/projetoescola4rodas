﻿using Escola4RodasTecnologia.Comum.Entity;
using Escola4RodasTecnologia.Escola4Rodas.Dominio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Escola4RodasTecnologia.Escola4Rodas.Entity.TypeConfiguration
{
    public class PassageiroTypeConfiguration : Escola4RodasEntityAbstractConfig<Passageiro>
    {
        protected override void ConfigurarCamposTabela()
        {
            Property(p => p.Codigo_Usuario)
                .IsRequired()
                .HasColumnName("codigo_usuario");

            Property(p => p.Codigo_Forma_Pagamento)
                .IsRequired()
                .HasColumnName("codigo_forma_pagamento");

            Property(p => p.Tipo_Passageiro)
                .IsRequired()
                .HasColumnName("tipo_passageiro");

            Property(p => p.Tipo_Viagem)
                .IsRequired()
                .HasColumnName("tipo_viagem");

            Property(p => p.Codigo_Motorista)
                .IsRequired()
                .HasColumnName("codigo_motorista");

            Property(p => p.Dthr)
                .IsRequired()
                .HasColumnName("dthr");
                
        }

        protected override void ConfigurarChaveEstrangeiraTabela()
        {
            HasMany(p => p.LocaisPassageiro)
                .WithRequired(p => p.Passageiro)
                .HasForeignKey(p => p.Codigo_Passageiro);

            HasMany(p => p.Pagamentos)
                .WithRequired(p => p.Passageiro)
                .HasForeignKey(p => p.Codigo_Passageiro);

            HasMany(p => p.Rotas)
                .WithMany(p => p.Passageiros)
                .Map(cs =>
                        {
                            cs.MapLeftKey("codigo_passageiro");
                            cs.MapRightKey("codigo_rota");
                            cs.ToTable("passageiro_rota");
                        });

            /*
            HasMany(p => p.Instituicoes)
                .WithMany(p => p.Passageiros)
                .Map(cs =>
                {
                    cs.MapLeftKey("codigo_passageiro");
                    cs.MapRightKey("codigo_instituicao");
                    cs.ToTable("passageiro_instituicao");
                });
                */
            HasMany(p => p.PassageiroInstituicao)
                .WithRequired(p => p.Passageiro)
                .HasForeignKey(p => p.Codigo_Passageiro);
            

            HasMany(p => p.Viagens)
                .WithMany(p => p.Passageiros)
                .Map(pv =>
                {
                    pv.MapLeftKey("codigo_passageiro");
                    pv.MapRightKey("codigo_viagem");
                    pv.ToTable("passageiro_viagem");
                });
            
            //Passageiro_Embarque_Viagem -> Tabela de N - M, armazena todos os embarques e desembarques dos passageiros

        }

        protected override void ConfigurarChavePrimariaTabela()
        {
            HasKey(p => p.Codigo_Usuario);
        }

        protected override void ConfigurarNomeTabela()
        {
            ToTable("Passageiro");
        }
    }
}
