﻿using Escola4RodasTecnologia.Comum.Entity;
using Escola4RodasTecnologia.Escola4Rodas.Dominio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Escola4RodasTecnologia.Escola4Rodas.Entity.TypeConfiguration
{
    public class RotaTypeConfiguration : Escola4RodasEntityAbstractConfig<Rota>
    {
        protected override void ConfigurarCamposTabela()
        {
            Property(p => p.Codigo)
                .IsRequired()
                .HasColumnName("codigo")
                .HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);

            Property(p => p.Nome)
                .IsRequired()
                .HasColumnName("nome");

            Property(p => p.Dthr)
                .IsRequired()
                .HasColumnName("dthr");
        }

        protected override void ConfigurarChaveEstrangeiraTabela()
        {
            
        }

        protected override void ConfigurarChavePrimariaTabela()
        {
            HasKey(p => p.Codigo);
        }

        protected override void ConfigurarNomeTabela()
        {
            ToTable("Rota");
        }
    }
}
