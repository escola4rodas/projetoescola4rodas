namespace Escola4RodasTecnologia.Escola4Rodas.Entity.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Escola4RodasTecnologia.Escola4Rodas.Entity.Context.Escola4RodasDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Escola4RodasTecnologia.Escola4Rodas.Entity.Context.Escola4RodasDbContext context)
        {
            Dominio.DiaSemana segundaFeira = new Dominio.DiaSemana
            {
                Dia_Semana = "segunda-feira"

            };

            Dominio.DiaSemana tercaFeira = new Dominio.DiaSemana
            {
                Dia_Semana = "ter�a-feira"
            };

            Dominio.DiaSemana quartaFeira = new Dominio.DiaSemana
            {
                Dia_Semana = "quarta-feira"
            };

            Dominio.DiaSemana quintaFeira = new Dominio.DiaSemana
            {
                Dia_Semana = "quinta-feira"
            };

            Dominio.DiaSemana sextaFeira = new Dominio.DiaSemana
            {
                Dia_Semana = "sexta-feira"
            };

            Dominio.DiaSemana sabado = new Dominio.DiaSemana
            {
                Dia_Semana = "s�bado"
            };

            Dominio.DiaSemana domingo = new Dominio.DiaSemana
            {
                Dia_Semana = "domingo"
            };

            Dominio.DiaSemanaLocal diaSemanaLocalSegunda = new Dominio.DiaSemanaLocal
            {
                Codigo = segundaFeira.Codigo,
                DiaSemana = segundaFeira
            };

            Dominio.DiaSemanaLocal diaSemanaLocalTerca = new Dominio.DiaSemanaLocal
            {
                Codigo = tercaFeira.Codigo,
                DiaSemana = tercaFeira
            };

            Dominio.DiaSemanaLocal diaSemanaLocalQuarta = new Dominio.DiaSemanaLocal
            {
                Codigo = quartaFeira.Codigo,
                DiaSemana = quartaFeira
            };

            Dominio.DiaSemanaLocal diaSemanaLocalQuinta = new Dominio.DiaSemanaLocal
            {
                Codigo = quintaFeira.Codigo,
                DiaSemana = quintaFeira
            };

            Dominio.DiaSemanaLocal diaSemanaLocalSexta = new Dominio.DiaSemanaLocal
            {
                Codigo = sextaFeira.Codigo,
                DiaSemana = sextaFeira
            };

            Dominio.DiaSemanaLocal diaSemanaLocalSabado = new Dominio.DiaSemanaLocal
            {
                Codigo = sabado.Codigo,
                DiaSemana = sabado
            };

            Dominio.DiaSemanaLocal diaSemanaLocalDomingo = new Dominio.DiaSemanaLocal
            {
                Codigo = domingo.Codigo,
                DiaSemana = domingo
            };

            Dominio.DiaSemanaViagem diaSemanaViagemSegunda = new Dominio.DiaSemanaViagem
            {
                Codigo = segundaFeira.Codigo,
                DiaSemana = segundaFeira
            };

            Dominio.DiaSemanaViagem diaSemanaViagemTerca = new Dominio.DiaSemanaViagem
            {
                Codigo = tercaFeira.Codigo,
                DiaSemana = tercaFeira
            };

            Dominio.DiaSemanaViagem diaSemanaViagemQuarta = new Dominio.DiaSemanaViagem
            {
                Codigo = quartaFeira.Codigo,
                DiaSemana = quartaFeira
            };

            Dominio.DiaSemanaViagem diaSemanaViagemQuinta = new Dominio.DiaSemanaViagem
            {
                Codigo = quintaFeira.Codigo,
                DiaSemana = quintaFeira
            };

            Dominio.DiaSemanaViagem diaSemanaViagemSexta = new Dominio.DiaSemanaViagem
            {
                Codigo = sextaFeira.Codigo,
                DiaSemana = sextaFeira
            };

            Dominio.DiaSemanaViagem diaSemanaViagemSabado = new Dominio.DiaSemanaViagem
            {
                Codigo = sabado.Codigo,
                DiaSemana = sabado
            };

            Dominio.DiaSemanaViagem diaSemanaViagemDomingo = new Dominio.DiaSemanaViagem
            {
                Codigo = domingo.Codigo,
                DiaSemana = domingo
            };

            Dominio.Usuario usuarioEderJames = new Dominio.Usuario
            {
                Nome = "Eder James",
                Idade = 22,
                Telefone = "5499126622",
                Login = "ederjames",
                Senha = "123456",
                Email = "edeleno12@gmail.com",
                Dthr = DateTime.Now
            };

            Dominio.Usuario usuarioMariaFernanda = new Dominio.Usuario
            {
                Nome = "Maria Fernanda",
                Idade = 22,
                Telefone = "5492995500",
                Login = "mariafernanda",
                Senha = "123456",
                Email = "feh@teste.com",
                Dthr = DateTime.Now
            };

            Dominio.Usuario usuarioDavid = new Dominio.Usuario
            {
                Nome = "David",
                Idade = 22,
                Telefone = "5492995500",
                Login = "david",
                Senha = "123456",
                Email = "david@teste.com",
                Dthr = DateTime.Now
            };

            Dominio.Usuario usuarioGabriel = new Dominio.Usuario
            {
                Nome = "Gabriel",
                Idade = 22,
                Telefone = "5492995500",
                Login = "gabriel",
                Senha = "123456",
                Email = "gabriel@teste.com",
                Dthr = DateTime.Now
            };

            Dominio.Usuario usuarioJoao = new Dominio.Usuario
            {
                Nome = "Joao",
                Idade = 50,
                Telefone = "5492995500",
                Login = "joao",
                Senha = "123456",
                Email = "joao@joao.com",
                Dthr = DateTime.Now
            };

            Dominio.Usuario usuarioJonata = new Dominio.Usuario
            {
                Nome = "Jonata",
                Idade = 28,
                Telefone = "5492995500",
                Login = "jonata",
                Senha = "123456",
                Email = "jonata@teste.com",
                Dthr = DateTime.Now
            };

            Dominio.Usuario usuarioPedro = new Dominio.Usuario
            {
                Nome = "Pedro",
                Idade = 35,
                Telefone = "5492995500",
                Login = "pedro",
                Senha = "123456",
                Email = "pedro@pedro.com",
                Dthr = DateTime.Now
            };

            Dominio.Usuario usuarioBruno = new Dominio.Usuario
            {
                Nome = "Bruno",
                Idade = 22,
                Telefone = "5492995500",
                Login = "bruno",
                Senha = "123456",
                Email = "bruno@bruno.com",
                Dthr = DateTime.Now
            };

            Dominio.Motorista motoristaEderJames = new Dominio.Motorista
            {
                //Codigo_Usuario = 1,
                Usuario = usuarioEderJames,
                Cnh = "123456789"
                // Passageiros = new List<Dominio.Passageiro> { passageiroJoao }
            };

            Dominio.Motorista motoristaMariaFernanda = new Dominio.Motorista
            {
                //Codigo_Usuario = 2,
                Usuario = usuarioMariaFernanda,
                Cnh = "123456789"
                //Passageiros = new List<Dominio.Passageiro> { passageiroPedro }
            };

            Dominio.Motorista motoristaGabriel = new Dominio.Motorista
            {
                //Codigo_Usuario = 2,
                Usuario = usuarioGabriel,
                Cnh = "123456789"
                //Passageiros = new List<Dominio.Passageiro> { passageiroJonata }
            };

            Dominio.Motorista motoristaDavid = new Dominio.Motorista
            {
                //Codigo_Usuario = 2,
                Usuario = usuarioDavid,
                Cnh = "123456789"
                //Passageiros = new List<Dominio.Passageiro> { passageiroBruno }
            };

            Dominio.Passageiro passageiroJoao = new Dominio.Passageiro
            {
                //Codigo_Usuario = 3,
                Usuario = usuarioJoao,
                Tipo_Viagem = 1,
                Codigo_Forma_Pagamento = 1,
                Tipo_Passageiro = 1,
                Dthr = DateTime.Now,
                Motorista = motoristaGabriel

            };

            Dominio.Passageiro passageiroPedro = new Dominio.Passageiro
            {
                //Codigo_Usuario = 3,
                Usuario = usuarioPedro,
                Tipo_Viagem = 1,
                Codigo_Forma_Pagamento = 1,
                Tipo_Passageiro = 1,
                Dthr = DateTime.Now,
                Motorista = motoristaEderJames
            };

            Dominio.Passageiro passageiroJonata = new Dominio.Passageiro
            {
                Usuario = usuarioJonata,
                Tipo_Viagem = 1,
                Codigo_Forma_Pagamento = 2,
                Tipo_Passageiro = 1,
                Dthr = DateTime.Now,
                Motorista = motoristaDavid
            };

            Dominio.Passageiro passageiroBruno = new Dominio.Passageiro
            {
                Usuario = usuarioBruno,
                Tipo_Viagem = 1,
                Codigo_Forma_Pagamento = 2,
                Tipo_Passageiro = 1,
                Dthr = DateTime.Now,
                Motorista = motoristaMariaFernanda
            };

            Dominio.Veiculo veiculoAndorinha = new Dominio.Veiculo
            {
                Nome = "Andorinha",
                Placa = "IAD-1234",
                Carga_Maxima = 8,
                //Codigo_Motorista = 1,
                Motorista = motoristaGabriel,
                Dthr = DateTime.Now
            };

            Dominio.Veiculo veiculoVanBranca = new Dominio.Veiculo
            {
                Nome = "Van Branca",
                Placa = "ILD-1554",
                Carga_Maxima = 8,
                //Codigo_Motorista = 1,
                Motorista = motoristaDavid,
                Dthr = DateTime.Now
            };

            Dominio.Veiculo veiculoVanVermelha = new Dominio.Veiculo
            {
                Nome = "Van Vermelha",
                Placa = "ILD-1554",
                Carga_Maxima = 20,
                //Codigo_Motorista = 1,
                Motorista = motoristaEderJames,
                Dthr = DateTime.Now
            };

            Dominio.Veiculo veiculoRenaulKid = new Dominio.Veiculo
            {
                Nome = "Van renault Kid",
                Placa = "ILD-1554",
                Carga_Maxima = 20,
                //Codigo_Motorista = 1,
                Motorista = motoristaMariaFernanda,
                Dthr = DateTime.Now
            };

            Dominio.DocumentoVeiculo documentoVeiculo1 = new Dominio.DocumentoVeiculo
            {
                Nome_Documento = "Seguro obrigat�rio",
                Descricao = "Seguro obrigat�rio de van",
                Validade = DateTime.Now,
                Dthr = DateTime.Now,
                Codigo_Tipo_Documento = 1,
                //Codigo_Veiculo = 1
                Veiculo = veiculoAndorinha
            };

            Dominio.DocumentoVeiculo documentoVeiculo2 = new Dominio.DocumentoVeiculo
            {
                Nome_Documento = "Seguro ederSeguros",
                Descricao = "Seguro da van",
                Validade = DateTime.Now,
                Dthr = DateTime.Now,
                Codigo_Tipo_Documento = 2,
                //Codigo_Veiculo = 2
                Veiculo = veiculoVanBranca
            };

            Dominio.DocumentoVeiculo documentoVeiculo3 = new Dominio.DocumentoVeiculo
            {
                Nome_Documento = "IPVA",
                Descricao = "IPVA do ve�culo",
                Validade = DateTime.Now,
                Dthr = DateTime.Now,
                Codigo_Tipo_Documento = 3,
                //Codigo_Veiculo = 1
                Veiculo = veiculoRenaulKid
            };

            Dominio.DocumentoVeiculo documentoVeiculo4 = new Dominio.DocumentoVeiculo
            {
                Nome_Documento = "IPVA",
                Descricao = "IPVA do ve�culo",
                Validade = DateTime.Now,
                Dthr = DateTime.Now,
                Codigo_Tipo_Documento = 3,
                //Codigo_Veiculo = 2
                Veiculo = veiculoVanVermelha
            };

            Dominio.Local localCasaJoazinho = new Dominio.Local
            {
                Latitude = Convert.ToDecimal(123456789123456.1),
                Longitude = Convert.ToDecimal(123456789123456.3),
                Nome_Local = "Casa Jo�ozinho",
                Bairro = "Santa Catarina",
                Nome_Rua = "Rua Teste",
                Numero = 123,
                Dthr = DateTime.Now
            };

            Dominio.Local localCasaJoaozinho2 = new Dominio.Local
            {
                Latitude = Convert.ToDecimal(123456789123456.1),
                Longitude = Convert.ToDecimal(123456789123456.3),
                Nome_Local = "Casa Jo�ozinho2",
                Bairro = "Marechal Floriano",
                Nome_Rua = "Rua Teste",
                Numero = 123,
                Dthr = DateTime.Now
            };

            Dominio.Local localCasaJoaozinho3 = new Dominio.Local
            {
                Latitude = Convert.ToDecimal(123456789123456.1),
                Longitude = Convert.ToDecimal(123456789123456.3),
                Nome_Local = "Casa Jo�ozinho3",
                Bairro = "Santa Catarina",
                Nome_Rua = "Rua Teste12",
                Numero = 123,
                Dthr = DateTime.Now
            };

            Dominio.Local localCasaJoaozinho4 = new Dominio.Local
            {
                Latitude = Convert.ToDecimal(123456789123456.1),
                Longitude = Convert.ToDecimal(123456789123456.3),
                Nome_Local = "Casa Jo�ozinho4",
                Bairro = "Centro",
                Nome_Rua = "Rua Teste4",
                Numero = 123456,
                Dthr = DateTime.Now
            };

            Dominio.Local localFsg = new Dominio.Local
            {
                Latitude = Convert.ToDecimal(123456789123456.1),
                Longitude = Convert.ToDecimal(123456789123456.3),
                Nome_Local = "Faculdade da Serra Ga�cha - FSG",
                Bairro = "Centro",
                Nome_Rua = "Rua da Fsg",
                Numero = 123456,
                Dthr = DateTime.Now
            };

            Dominio.Local localUcs = new Dominio.Local
            {
                Latitude = Convert.ToDecimal(123456789123456.1),
                Longitude = Convert.ToDecimal(123456789123456.3),
                Nome_Local = "Universidade de Caxias do Sul - UCS",
                Bairro = "Cidade Universit�ria",
                Nome_Rua = "Rua da UCS",
                Numero = 123456,
                Dthr = DateTime.Now
            };

            Dominio.Local localFtec = new Dominio.Local
            {
                Latitude = Convert.ToDecimal(123456789123456.1),
                Longitude = Convert.ToDecimal(123456789123456.3),
                Nome_Local = "Centro universit�rio ftec - UniFtec",
                Bairro = "Cinquenten�rio",
                Nome_Rua = "Rua Gustavo Ramos Sebbe",
                Numero = 123456,
                Dthr = DateTime.Now
            };

            Dominio.Local localCristovao = new Dominio.Local
            {
                Latitude = Convert.ToDecimal(123456789123456.1),
                Longitude = Convert.ToDecimal(123456789123456.3),
                Nome_Local = "I.E.E - Crist�v�o de Mendoza",
                Bairro = "Cinquenten�rio",
                Nome_Rua = "Rua Gustavo Ramos Sebbe",
                Numero = 123456,
                Dthr = DateTime.Now
            };

            Dominio.LocalPassageiro localPassageiroJoaozinho = new Dominio.LocalPassageiro
            {
                //Codigo_Local = localCasaJoazinho.Codigo,
                //Codigo_Passageiro = passageiroJoao.Codigo_Usuario,
                Codigo_Tipo_Local = 1,
                Local = localCasaJoazinho,
                Passageiro = passageiroJoao,
                DiasSemanaLocal = new List<Dominio.DiaSemanaLocal> { diaSemanaLocalSegunda, diaSemanaLocalTerca, diaSemanaLocalQuarta }
            };

            Dominio.LocalPassageiro localPassageiroBruno = new Dominio.LocalPassageiro
            {
                //Codigo_Local = localCasaJoaozinho2.Codigo,
                //Codigo_Passageiro = passageiroBruno.Codigo_Usuario,
                Codigo_Tipo_Local = 1,
                Local = localCasaJoaozinho2,
                Passageiro = passageiroBruno,
                DiasSemanaLocal = new List<Dominio.DiaSemanaLocal> { diaSemanaLocalSegunda, diaSemanaLocalTerca, diaSemanaLocalQuarta }
            };

            Dominio.LocalPassageiro localPassageiroJonata = new Dominio.LocalPassageiro
            {
                //Codigo_Local = localCasaJoaozinho3.Codigo,
                //Codigo_Passageiro = passageiroBruno.Codigo_Usuario,
                Codigo_Tipo_Local = 1,
                Local = localCasaJoaozinho3,
                Passageiro = passageiroJonata,
                DiasSemanaLocal = new List<Dominio.DiaSemanaLocal> { diaSemanaLocalSegunda, diaSemanaLocalTerca, diaSemanaLocalSexta }
            };

            Dominio.LocalPassageiro localPassageiroPassageiroPedro = new Dominio.LocalPassageiro
            {
                //Codigo_Local = localCasaJoaozinho4.Codigo,
                //Codigo_Passageiro = passageiroPedro.Codigo_Usuario,
                Codigo_Tipo_Local = 1,
                Local = localCasaJoaozinho4,
                Passageiro = passageiroPedro,
                DiasSemanaLocal = new List<Dominio.DiaSemanaLocal> { diaSemanaLocalSegunda, diaSemanaLocalTerca, diaSemanaLocalQuarta } 
            };

            Dominio.Instituicao instituicaoFsg = new Dominio.Instituicao
            {
                Nome = "UniFsg - Centro Universit�rio Faculdade de Serra Ga�cha",
                Motoristas = new List<Dominio.Motorista>() { motoristaEderJames, motoristaMariaFernanda },
                Dthr = DateTime.Now,
                PassageiroInstituicao = new List<Dominio.PassageiroInstituicao>(),
                Codigo_Local = localFsg.Codigo,  
                
            };
            
            instituicaoFsg.LocalInstituicao = new Dominio.LocalInstituicao
            {
                Instituicao = instituicaoFsg,
                Local = localFsg,
                Codigo_Instituicao = instituicaoFsg.Codigo_Instituicao,
                Codigo_Local = localFsg.Codigo

            };

            Dominio.Instituicao instituicaoUcs = new Dominio.Instituicao
            {
                Nome = "UCS - Universidade de Caxias do Sul",
                Motoristas = new List<Dominio.Motorista>() { motoristaEderJames, motoristaMariaFernanda },
                Dthr = DateTime.Now,
                PassageiroInstituicao = new List<Dominio.PassageiroInstituicao>(),
                Codigo_Local = localUcs.Codigo
            };

            instituicaoUcs.LocalInstituicao = new Dominio.LocalInstituicao
            {
                Instituicao = instituicaoUcs,
                Local = localUcs,
                Codigo_Instituicao = instituicaoUcs.Codigo_Instituicao,
                Codigo_Local = localUcs.Codigo
            };

            Dominio.Instituicao instituicaoUniFtec = new Dominio.Instituicao
            {
                Nome = "UniFtec - Centro Universit�rio Ftec",
                Motoristas = new List<Dominio.Motorista>() { motoristaEderJames, motoristaMariaFernanda },
                Dthr = DateTime.Now,
                PassageiroInstituicao = new List<Dominio.PassageiroInstituicao>(),
                Codigo_Local = localFtec.Codigo
            };

            instituicaoUniFtec.LocalInstituicao = new Dominio.LocalInstituicao
            {

                Instituicao = instituicaoUniFtec,
                Local = localFtec,
                Codigo_Instituicao = instituicaoUniFtec.Codigo_Instituicao,
                Codigo_Local = localFtec.Codigo,
            };

            Dominio.Instituicao instituicaoCristovao = new Dominio.Instituicao
            {
                Nome = "Escola Crist�v�o de Mendoza",
                Motoristas = new List<Dominio.Motorista>() { motoristaEderJames, motoristaMariaFernanda },
                Dthr = DateTime.Now,
                PassageiroInstituicao = new List<Dominio.PassageiroInstituicao>(),
                Codigo_Local = localCristovao.Codigo
            };

            instituicaoCristovao.LocalInstituicao = new Dominio.LocalInstituicao
            {
                Instituicao = instituicaoCristovao,
                Local = localCristovao,
                Codigo_Instituicao = instituicaoCristovao.Codigo_Instituicao,
                Codigo_Local = localCristovao.Codigo
            };
            

            Dominio.PassageiroInstituicao passageiroInstituicaoBrunoCristovao = new Dominio.PassageiroInstituicao
            {
                Instituicao = instituicaoCristovao,
                Passageiro = passageiroBruno,
                Codigo_Instituicao = instituicaoCristovao.Codigo_Instituicao,
                Codigo_Passageiro = passageiroBruno.Codigo_Usuario,
                Codigo_Tipo_Passageiro = 1

            };

            Dominio.PassageiroInstituicao passageiroInstituicaoJoaoCristovao = new Dominio.PassageiroInstituicao
            {
                Instituicao = instituicaoCristovao,
                Passageiro = passageiroJoao,
                Codigo_Instituicao = instituicaoCristovao.Codigo_Instituicao,
                Codigo_Passageiro = passageiroJoao.Codigo_Usuario,
                Codigo_Tipo_Passageiro = 1
            };
            
           
            Dominio.Viagem viagemEscolar = new Dominio.Viagem
            {
                Nome = "Viagem Escolar",
                Data_Inicio = DateTime.Now,
                Tipo_Viagem = 1,
                Codigo_Dia_Semana = 1,
                Codigo_Veiculo = veiculoAndorinha.Codigo_Veiculo,
                VeiculoViagem = veiculoAndorinha,
                Dthr = DateTime.Now,
                Passageiros = new List<Dominio.Passageiro> { passageiroJoao, passageiroPedro, passageiroJonata },
                DiaSemanaViagem = new List<Dominio.DiaSemanaViagem> { diaSemanaViagemSegunda, diaSemanaViagemTerca },
                Instituicoes = new List<Dominio.Instituicao> { instituicaoCristovao, instituicaoUniFtec }
            };

            Dominio.Viagem viagemFaculdades = new Dominio.Viagem
            {
                Nome = "Viagem Faculdades",
                Data_Inicio = DateTime.Now,
                Tipo_Viagem = 1,
                Codigo_Dia_Semana = 1,
                Codigo_Veiculo = veiculoVanBranca.Codigo_Veiculo,
                VeiculoViagem = veiculoVanBranca,
                Dthr = DateTime.Now,
                Passageiros = new List<Dominio.Passageiro> { passageiroJoao, passageiroPedro, passageiroJonata },
                DiaSemanaViagem = new List<Dominio.DiaSemanaViagem> { diaSemanaViagemQuarta, diaSemanaViagemSexta },
                Instituicoes = new List<Dominio.Instituicao> { instituicaoFsg, instituicaoUniFtec}
            };

            Dominio.Pagamento pagamentoJaneiro2016Joao = new Dominio.Pagamento {
                Ano = 2016,
                Codigo_Passageiro = passageiroJoao.Codigo_Usuario,
                Descricao = "Pagamento do jo�o em janeiro de 2016",
                Dia = 8,
                Dthr = DateTime.Now,
                Mes = 1,
                Valor = 180,
                Passageiro = passageiroJoao
            };

            Dominio.Pagamento pagamentoFevereiro2016Joao = new Dominio.Pagamento
            {
                Ano = 2016,
                Codigo_Passageiro = passageiroJoao.Codigo_Usuario,
                Descricao = "Pagamento do jo�o em fevereiro de 2016",
                Dia = 8,
                Dthr = DateTime.Now,
                Mes = 2,
                Valor = 175.50,
                Passageiro = passageiroJoao
            };

            Dominio.Pagamento pagamentoMarco2016Joao = new Dominio.Pagamento
            {
                Ano = 2016,
                Codigo_Passageiro = passageiroJoao.Codigo_Usuario,
                Descricao = "Pagamento do jo�o em mar�o de 2016",
                Dia = 8,
                Dthr = DateTime.Now,
                Mes = 3,
                Valor = 150.50,
                Passageiro = passageiroJoao
            };

            Dominio.Pagamento pagamentoJaneiro2017Joao = new Dominio.Pagamento
            {
                Ano = 2017,
                Codigo_Passageiro = passageiroJoao.Codigo_Usuario,
                Descricao = "Pagamento do jo�o em janeiro de 2017",
                Dia = 8,
                Dthr = DateTime.Now,
                Mes = 1,
                Valor = 150.50,
                Passageiro = passageiroJoao
            };

            Dominio.Pagamento pagamentoFevereiro2017Joao = new Dominio.Pagamento
            {
                Ano = 2017,
                Codigo_Passageiro = passageiroJoao.Codigo_Usuario,
                Descricao = "Pagamento do jo�o em janeiro de 2017",
                Dia = 8,
                Dthr = DateTime.Now,
                Mes = 2,
                Valor = 165.50,
                Passageiro = passageiroJoao
            };

            Dominio.Pagamento pagamentoMarco2017Joao = new Dominio.Pagamento
            {
                Ano = 2017,
                Codigo_Passageiro = passageiroJoao.Codigo_Usuario,
                Descricao = "Pagamento do jo�o em janeiro de 2017",
                Dia = 8,
                Dthr = DateTime.Now,
                Mes = 3,
                Valor = 180.50,
                Passageiro = passageiroJoao
            };

            //
            Dominio.Pagamento pagamentoJaneiro2016Pedro = new Dominio.Pagamento
            {
                Ano = 2016,
                Codigo_Passageiro = passageiroPedro.Codigo_Usuario,
                Descricao = "Pagamento do pedro em janeiro de 2016",
                Dia = 8,
                Dthr = DateTime.Now,
                Mes = 1,
                Valor = 180,
                Passageiro = passageiroPedro
            };

            Dominio.Pagamento pagamentoFevereiro2016Pedro = new Dominio.Pagamento
            {
                Ano = 2016,
                Codigo_Passageiro = passageiroPedro.Codigo_Usuario,
                Descricao = "Pagamento do pedro em fevereiro de 2016",
                Dia = 8,
                Dthr = DateTime.Now,
                Mes = 2,
                Valor = 175.50,
                Passageiro = passageiroPedro
            };

            Dominio.Pagamento pagamentoMarco2016Pedro = new Dominio.Pagamento
            {
                Ano = 2016,
                Codigo_Passageiro = passageiroPedro.Codigo_Usuario,
                Descricao = "Pagamento do pedro em mar�o de 2016",
                Dia = 8,
                Dthr = DateTime.Now,
                Mes = 3,
                Valor = 150.50,
                Passageiro = passageiroPedro
            };

            Dominio.Pagamento pagamentoJaneiro2017Pedro = new Dominio.Pagamento
            {
                Ano = 2017,
                Codigo_Passageiro = passageiroPedro.Codigo_Usuario,
                Descricao = "Pagamento do pedro em janeiro de 2017",
                Dia = 8,
                Dthr = DateTime.Now,
                Mes = 1,
                Valor = 150.50,
                Passageiro = passageiroPedro
            };

            Dominio.Pagamento pagamentoFevereiro2017Pedro = new Dominio.Pagamento
            {
                Ano = 2017,
                Codigo_Passageiro = passageiroPedro.Codigo_Usuario,
                Descricao = "Pagamento do pedro em fevereiro de 2017",
                Dia = 8,
                Dthr = DateTime.Now,
                Mes = 2,
                Valor = 165.50,
                Passageiro = passageiroPedro
            };

            Dominio.Pagamento pagamentoMarco2017Pedro = new Dominio.Pagamento
            {
                Ano = 2017,
                Codigo_Passageiro = passageiroPedro.Codigo_Usuario,
                Descricao = "Pagamento do pedro em mar�o de 2017",
                Dia = 8,
                Dthr = DateTime.Now,
                Mes = 3,
                Valor = 180.50,
                Passageiro = passageiroPedro
            };

            context.DiaSemana.Add(segundaFeira);
            context.DiaSemana.Add(tercaFeira);
            context.DiaSemana.Add(quartaFeira);
            context.DiaSemana.Add(quintaFeira);
            context.DiaSemana.Add(sextaFeira);
            context.DiaSemana.Add(sabado);
            context.DiaSemana.Add(domingo);

            context.DiaSemanaLocal.Add(diaSemanaLocalSegunda);
            context.DiaSemanaLocal.Add(diaSemanaLocalTerca);
            context.DiaSemanaLocal.Add(diaSemanaLocalQuarta);
            context.DiaSemanaLocal.Add(diaSemanaLocalQuinta);
            context.DiaSemanaLocal.Add(diaSemanaLocalSexta);
            context.DiaSemanaLocal.Add(diaSemanaLocalSabado);
            context.DiaSemanaLocal.Add(diaSemanaLocalDomingo);

            context.DiaSemanaViagem.Add(diaSemanaViagemSegunda);
            context.DiaSemanaViagem.Add(diaSemanaViagemTerca);
            context.DiaSemanaViagem.Add(diaSemanaViagemQuarta);
            context.DiaSemanaViagem.Add(diaSemanaViagemQuinta);
            context.DiaSemanaViagem.Add(diaSemanaViagemSexta);
            context.DiaSemanaViagem.Add(diaSemanaViagemSabado);
            context.DiaSemanaViagem.Add(diaSemanaViagemDomingo);

            context.Usuario.Add(usuarioEderJames);
            context.Usuario.Add(usuarioJoao);
            context.Usuario.Add(usuarioMariaFernanda);
            context.Usuario.Add(usuarioPedro);
            context.Usuario.Add(usuarioBruno);
            context.Usuario.Add(usuarioDavid);
            context.Usuario.Add(usuarioJonata);

            context.Passageiro.Add(passageiroJoao);
            context.Passageiro.Add(passageiroPedro);
            context.Passageiro.Add(passageiroJonata);
            context.Passageiro.Add(passageiroBruno);

            context.Motorista.Add(motoristaEderJames);
            context.Motorista.Add(motoristaMariaFernanda);
            context.Motorista.Add(motoristaGabriel);
            context.Motorista.Add(motoristaDavid);

            context.Veiculo.Add(veiculoAndorinha);
            context.Veiculo.Add(veiculoVanBranca);
            context.Veiculo.Add(veiculoVanVermelha);

            context.DocumentoVeiculo.Add(documentoVeiculo1);
            context.DocumentoVeiculo.Add(documentoVeiculo2);
            context.DocumentoVeiculo.Add(documentoVeiculo3);
            context.DocumentoVeiculo.Add(documentoVeiculo4);

            context.Local.Add(localCasaJoazinho);
            context.Local.Add(localCasaJoaozinho2);
            context.Local.Add(localCasaJoaozinho3);
            context.Local.Add(localCasaJoaozinho4);
            context.Local.Add(localCristovao);
            context.Local.Add(localFsg);
            context.Local.Add(localFtec);
            context.Local.Add(localUcs);

            context.Instituicao.Add(instituicaoCristovao);
            context.Instituicao.Add(instituicaoFsg);
            context.Instituicao.Add(instituicaoUcs);
            context.Instituicao.Add(instituicaoUniFtec);

            context.LocalPassageiro.Add(localPassageiroJoaozinho);
            context.LocalPassageiro.Add(localPassageiroBruno);
            context.LocalPassageiro.Add(localPassageiroJonata);
            context.LocalPassageiro.Add(localPassageiroPassageiroPedro);

            /*
            context.LocalInstituicao.Add(localInstituicaoFsg);
            context.LocalInstituicao.Add(localInstituicaoFtec);
            context.LocalInstituicao.Add(localInstituicaoCristovao);
            context.LocalInstituicao.Add(localInstituicaoUcs);
            */            

            context.PassageiroInstituicao.Add(passageiroInstituicaoBrunoCristovao);
            context.PassageiroInstituicao.Add(passageiroInstituicaoJoaoCristovao);

            context.Viagem.Add(viagemEscolar);
            context.Viagem.Add(viagemFaculdades);
            
            context.Pagamento.Add(pagamentoJaneiro2016Joao);
            context.Pagamento.Add(pagamentoFevereiro2016Joao);
            context.Pagamento.Add(pagamentoMarco2016Joao);
            context.Pagamento.Add(pagamentoJaneiro2017Joao);
            context.Pagamento.Add(pagamentoFevereiro2017Joao);
            context.Pagamento.Add(pagamentoMarco2017Joao);

            context.Pagamento.Add(pagamentoJaneiro2016Pedro);
            context.Pagamento.Add(pagamentoFevereiro2016Pedro);
            context.Pagamento.Add(pagamentoMarco2016Pedro);
            context.Pagamento.Add(pagamentoJaneiro2017Pedro);
            context.Pagamento.Add(pagamentoFevereiro2017Pedro);
            context.Pagamento.Add(pagamentoMarco2017Pedro);

            //context.SaveChanges();
            context.Configuration.LazyLoadingEnabled = false;
            base.Seed(context);
        }
    }
}
