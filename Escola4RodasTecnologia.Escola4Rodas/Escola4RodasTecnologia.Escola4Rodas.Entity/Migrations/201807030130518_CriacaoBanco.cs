namespace Escola4RodasTecnologia.Escola4Rodas.Entity.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CriacaoBanco : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Dia_Semana",
                c => new
                    {
                        codigo = c.Int(nullable: false, identity: true),
                        dia_semana = c.String(nullable: false, maxLength: 13),
                    })
                .PrimaryKey(t => t.codigo);
            
            CreateTable(
                "dbo.Dia_Semana_Local",
                c => new
                    {
                        codigo_dia_semana = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.codigo_dia_semana)
                .ForeignKey("dbo.Dia_Semana", t => t.codigo_dia_semana)
                .Index(t => t.codigo_dia_semana);
            
            CreateTable(
                "dbo.Local_Passageiro",
                c => new
                    {
                        codigo_local = c.Int(nullable: false),
                        codigo_tipo_local = c.Int(nullable: false),
                        codigo_passageiro = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.codigo_local)
                .ForeignKey("dbo.Passageiro", t => t.codigo_passageiro)
                .ForeignKey("dbo.local", t => t.codigo_local)
                .Index(t => t.codigo_local)
                .Index(t => t.codigo_passageiro);
            
            CreateTable(
                "dbo.local",
                c => new
                    {
                        codigo = c.Int(nullable: false, identity: true),
                        latitude = c.Decimal(nullable: false, precision: 18, scale: 2),
                        longitude = c.Decimal(nullable: false, precision: 18, scale: 2),
                        nome_local = c.String(nullable: false),
                        bairro = c.String(nullable: false, maxLength: 50),
                        rua = c.String(nullable: false, maxLength: 50),
                        numero = c.Int(nullable: false),
                        Dthr = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.codigo);
            
            CreateTable(
                "dbo.Local_Instituicao",
                c => new
                    {
                        codigo_local = c.Int(nullable: false),
                        Codigo_Instituicao = c.Int(nullable: false),
                        Instituicao_Codigo_Instituicao = c.Int(),
                    })
                .PrimaryKey(t => t.codigo_local)
                .ForeignKey("dbo.instituicao", t => t.Instituicao_Codigo_Instituicao)
                .ForeignKey("dbo.local", t => t.codigo_local)
                .Index(t => t.codigo_local)
                .Index(t => t.Instituicao_Codigo_Instituicao);
            
            CreateTable(
                "dbo.instituicao",
                c => new
                    {
                        codigo_instituicao = c.Int(nullable: false, identity: true),
                        nome = c.String(nullable: false),
                        Codigo_Local = c.Int(nullable: false),
                        dthr = c.DateTime(nullable: false),
                        Codigo_Local_Instituicao = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.codigo_instituicao);
            
            CreateTable(
                "dbo.Motorista",
                c => new
                    {
                        codigo_usuario = c.Int(nullable: false),
                        cnh = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.codigo_usuario)
                .ForeignKey("dbo.Usuario", t => t.codigo_usuario)
                .Index(t => t.codigo_usuario);
            
            CreateTable(
                "dbo.Passageiro",
                c => new
                    {
                        codigo_usuario = c.Int(nullable: false),
                        tipo_viagem = c.Int(nullable: false),
                        codigo_forma_pagamento = c.Int(nullable: false),
                        tipo_passageiro = c.Int(nullable: false),
                        dthr = c.DateTime(nullable: false),
                        codigo_motorista = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.codigo_usuario)
                .ForeignKey("dbo.Usuario", t => t.codigo_usuario)
                .ForeignKey("dbo.Motorista", t => t.codigo_motorista)
                .Index(t => t.codigo_usuario)
                .Index(t => t.codigo_motorista);
            
            CreateTable(
                "dbo.pagamento",
                c => new
                    {
                        codigo = c.Int(nullable: false, identity: true),
                        descricao = c.String(),
                        dthr = c.DateTime(nullable: false),
                        codigo_passageiro = c.Int(nullable: false),
                        mes = c.Int(nullable: false),
                        ano = c.Int(nullable: false),
                        Dia = c.Int(nullable: false),
                        valor = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.codigo)
                .ForeignKey("dbo.Passageiro", t => t.codigo_passageiro)
                .Index(t => t.codigo_passageiro);
            
            CreateTable(
                "dbo.Passageiro_Instituicao",
                c => new
                    {
                        codigo_passageiro = c.Int(nullable: false),
                        codigo_instituicao = c.Int(nullable: false),
                        codigo_tipo_passageiro = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.codigo_passageiro, t.codigo_instituicao })
                .ForeignKey("dbo.Passageiro", t => t.codigo_passageiro)
                .ForeignKey("dbo.instituicao", t => t.codigo_instituicao)
                .Index(t => t.codigo_passageiro)
                .Index(t => t.codigo_instituicao);
            
            CreateTable(
                "dbo.Rota",
                c => new
                    {
                        codigo = c.Int(nullable: false, identity: true),
                        nome = c.String(nullable: false),
                        dthr = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.codigo);
            
            CreateTable(
                "dbo.Usuario",
                c => new
                    {
                        codigo = c.Int(nullable: false, identity: true),
                        nome = c.String(nullable: false, maxLength: 100),
                        idade = c.Int(nullable: false),
                        telefone = c.String(nullable: false, maxLength: 22),
                        login = c.String(nullable: false, maxLength: 20),
                        senha = c.String(nullable: false),
                        email = c.String(nullable: false, maxLength: 50),
                        Dthr = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.codigo);
            
            CreateTable(
                "dbo.viagem",
                c => new
                    {
                        codigo = c.Int(nullable: false, identity: true),
                        nome = c.String(nullable: false),
                        codigo_veiculo = c.Int(nullable: false),
                        data_inicio = c.DateTime(nullable: false),
                        dthr = c.DateTime(nullable: false),
                        Tipo_Viagem = c.Int(nullable: false),
                        codigo_dia_semana = c.Int(nullable: false),
                        Motorista_Codigo_Usuario = c.Int(),
                    })
                .PrimaryKey(t => t.codigo)
                .ForeignKey("dbo.veiculo", t => t.codigo_veiculo)
                .ForeignKey("dbo.Motorista", t => t.Motorista_Codigo_Usuario)
                .Index(t => t.codigo_veiculo)
                .Index(t => t.Motorista_Codigo_Usuario);
            
            CreateTable(
                "dbo.Dia_Semana_Viagem",
                c => new
                    {
                        codigo_dia_semana = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.codigo_dia_semana)
                .ForeignKey("dbo.Dia_Semana", t => t.codigo_dia_semana)
                .Index(t => t.codigo_dia_semana);
            
            CreateTable(
                "dbo.veiculo",
                c => new
                    {
                        codigo_veiculo = c.Int(nullable: false, identity: true),
                        nome = c.String(nullable: false),
                        placa = c.String(nullable: false),
                        carga_maxima = c.Int(nullable: false),
                        codigo_motorista = c.Int(nullable: false),
                        dthr = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.codigo_veiculo)
                .ForeignKey("dbo.Motorista", t => t.codigo_motorista)
                .Index(t => t.codigo_motorista);
            
            CreateTable(
                "dbo.documento_veiculo",
                c => new
                    {
                        codigo = c.Int(nullable: false, identity: true),
                        nome_documento = c.String(nullable: false),
                        descricao = c.String(maxLength: 100),
                        validade = c.DateTime(nullable: false),
                        Dthr = c.DateTime(nullable: false),
                        tipo_documento = c.Int(nullable: false),
                        codigo_veiculo = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.codigo)
                .ForeignKey("dbo.veiculo", t => t.codigo_veiculo)
                .Index(t => t.codigo_veiculo);
            
            CreateTable(
                "dbo.localpassageiro_dia_semana",
                c => new
                    {
                        codigo_local = c.Int(nullable: false),
                        codigo_dia_semana = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.codigo_local, t.codigo_dia_semana })
                .ForeignKey("dbo.Local_Passageiro", t => t.codigo_local)
                .ForeignKey("dbo.Dia_Semana_Local", t => t.codigo_dia_semana)
                .Index(t => t.codigo_local)
                .Index(t => t.codigo_dia_semana);
            
            CreateTable(
                "dbo.motorista_instituicao",
                c => new
                    {
                        codigo_motorista = c.Int(nullable: false),
                        codigo_instituicao = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.codigo_motorista, t.codigo_instituicao })
                .ForeignKey("dbo.Motorista", t => t.codigo_motorista)
                .ForeignKey("dbo.instituicao", t => t.codigo_instituicao)
                .Index(t => t.codigo_motorista)
                .Index(t => t.codigo_instituicao);
            
            CreateTable(
                "dbo.passageiro_rota",
                c => new
                    {
                        codigo_passageiro = c.Int(nullable: false),
                        codigo_rota = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.codigo_passageiro, t.codigo_rota })
                .ForeignKey("dbo.Passageiro", t => t.codigo_passageiro)
                .ForeignKey("dbo.Rota", t => t.codigo_rota)
                .Index(t => t.codigo_passageiro)
                .Index(t => t.codigo_rota);
            
            CreateTable(
                "dbo.viagem_dia_semana",
                c => new
                    {
                        codigo_viagem = c.Int(nullable: false),
                        codigo_dia_semana = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.codigo_viagem, t.codigo_dia_semana })
                .ForeignKey("dbo.viagem", t => t.codigo_viagem)
                .ForeignKey("dbo.Dia_Semana_Viagem", t => t.codigo_dia_semana)
                .Index(t => t.codigo_viagem)
                .Index(t => t.codigo_dia_semana);
            
            CreateTable(
                "dbo.viagem_instituicao",
                c => new
                    {
                        codigo_viagem = c.Int(nullable: false),
                        codigo_instituicao = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.codigo_viagem, t.codigo_instituicao })
                .ForeignKey("dbo.viagem", t => t.codigo_viagem)
                .ForeignKey("dbo.instituicao", t => t.codigo_instituicao)
                .Index(t => t.codigo_viagem)
                .Index(t => t.codigo_instituicao);
            
            CreateTable(
                "dbo.passageiro_viagem",
                c => new
                    {
                        codigo_passageiro = c.Int(nullable: false),
                        codigo_viagem = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.codigo_passageiro, t.codigo_viagem })
                .ForeignKey("dbo.Passageiro", t => t.codigo_passageiro)
                .ForeignKey("dbo.viagem", t => t.codigo_viagem)
                .Index(t => t.codigo_passageiro)
                .Index(t => t.codigo_viagem);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Dia_Semana_Viagem", "codigo_dia_semana", "dbo.Dia_Semana");
            DropForeignKey("dbo.Dia_Semana_Local", "codigo_dia_semana", "dbo.Dia_Semana");
            DropForeignKey("dbo.Local_Passageiro", "codigo_local", "dbo.local");
            DropForeignKey("dbo.Local_Instituicao", "codigo_local", "dbo.local");
            DropForeignKey("dbo.Passageiro_Instituicao", "codigo_instituicao", "dbo.instituicao");
            DropForeignKey("dbo.viagem", "Motorista_Codigo_Usuario", "dbo.Motorista");
            DropForeignKey("dbo.veiculo", "codigo_motorista", "dbo.Motorista");
            DropForeignKey("dbo.Passageiro", "codigo_motorista", "dbo.Motorista");
            DropForeignKey("dbo.passageiro_viagem", "codigo_viagem", "dbo.viagem");
            DropForeignKey("dbo.passageiro_viagem", "codigo_passageiro", "dbo.Passageiro");
            DropForeignKey("dbo.viagem", "codigo_veiculo", "dbo.veiculo");
            DropForeignKey("dbo.documento_veiculo", "codigo_veiculo", "dbo.veiculo");
            DropForeignKey("dbo.viagem_instituicao", "codigo_instituicao", "dbo.instituicao");
            DropForeignKey("dbo.viagem_instituicao", "codigo_viagem", "dbo.viagem");
            DropForeignKey("dbo.viagem_dia_semana", "codigo_dia_semana", "dbo.Dia_Semana_Viagem");
            DropForeignKey("dbo.viagem_dia_semana", "codigo_viagem", "dbo.viagem");
            DropForeignKey("dbo.Passageiro", "codigo_usuario", "dbo.Usuario");
            DropForeignKey("dbo.Motorista", "codigo_usuario", "dbo.Usuario");
            DropForeignKey("dbo.passageiro_rota", "codigo_rota", "dbo.Rota");
            DropForeignKey("dbo.passageiro_rota", "codigo_passageiro", "dbo.Passageiro");
            DropForeignKey("dbo.Passageiro_Instituicao", "codigo_passageiro", "dbo.Passageiro");
            DropForeignKey("dbo.pagamento", "codigo_passageiro", "dbo.Passageiro");
            DropForeignKey("dbo.Local_Passageiro", "codigo_passageiro", "dbo.Passageiro");
            DropForeignKey("dbo.motorista_instituicao", "codigo_instituicao", "dbo.instituicao");
            DropForeignKey("dbo.motorista_instituicao", "codigo_motorista", "dbo.Motorista");
            DropForeignKey("dbo.Local_Instituicao", "Instituicao_Codigo_Instituicao", "dbo.instituicao");
            DropForeignKey("dbo.localpassageiro_dia_semana", "codigo_dia_semana", "dbo.Dia_Semana_Local");
            DropForeignKey("dbo.localpassageiro_dia_semana", "codigo_local", "dbo.Local_Passageiro");
            DropIndex("dbo.passageiro_viagem", new[] { "codigo_viagem" });
            DropIndex("dbo.passageiro_viagem", new[] { "codigo_passageiro" });
            DropIndex("dbo.viagem_instituicao", new[] { "codigo_instituicao" });
            DropIndex("dbo.viagem_instituicao", new[] { "codigo_viagem" });
            DropIndex("dbo.viagem_dia_semana", new[] { "codigo_dia_semana" });
            DropIndex("dbo.viagem_dia_semana", new[] { "codigo_viagem" });
            DropIndex("dbo.passageiro_rota", new[] { "codigo_rota" });
            DropIndex("dbo.passageiro_rota", new[] { "codigo_passageiro" });
            DropIndex("dbo.motorista_instituicao", new[] { "codigo_instituicao" });
            DropIndex("dbo.motorista_instituicao", new[] { "codigo_motorista" });
            DropIndex("dbo.localpassageiro_dia_semana", new[] { "codigo_dia_semana" });
            DropIndex("dbo.localpassageiro_dia_semana", new[] { "codigo_local" });
            DropIndex("dbo.documento_veiculo", new[] { "codigo_veiculo" });
            DropIndex("dbo.veiculo", new[] { "codigo_motorista" });
            DropIndex("dbo.Dia_Semana_Viagem", new[] { "codigo_dia_semana" });
            DropIndex("dbo.viagem", new[] { "Motorista_Codigo_Usuario" });
            DropIndex("dbo.viagem", new[] { "codigo_veiculo" });
            DropIndex("dbo.Passageiro_Instituicao", new[] { "codigo_instituicao" });
            DropIndex("dbo.Passageiro_Instituicao", new[] { "codigo_passageiro" });
            DropIndex("dbo.pagamento", new[] { "codigo_passageiro" });
            DropIndex("dbo.Passageiro", new[] { "codigo_motorista" });
            DropIndex("dbo.Passageiro", new[] { "codigo_usuario" });
            DropIndex("dbo.Motorista", new[] { "codigo_usuario" });
            DropIndex("dbo.Local_Instituicao", new[] { "Instituicao_Codigo_Instituicao" });
            DropIndex("dbo.Local_Instituicao", new[] { "codigo_local" });
            DropIndex("dbo.Local_Passageiro", new[] { "codigo_passageiro" });
            DropIndex("dbo.Local_Passageiro", new[] { "codigo_local" });
            DropIndex("dbo.Dia_Semana_Local", new[] { "codigo_dia_semana" });
            DropTable("dbo.passageiro_viagem");
            DropTable("dbo.viagem_instituicao");
            DropTable("dbo.viagem_dia_semana");
            DropTable("dbo.passageiro_rota");
            DropTable("dbo.motorista_instituicao");
            DropTable("dbo.localpassageiro_dia_semana");
            DropTable("dbo.documento_veiculo");
            DropTable("dbo.veiculo");
            DropTable("dbo.Dia_Semana_Viagem");
            DropTable("dbo.viagem");
            DropTable("dbo.Usuario");
            DropTable("dbo.Rota");
            DropTable("dbo.Passageiro_Instituicao");
            DropTable("dbo.pagamento");
            DropTable("dbo.Passageiro");
            DropTable("dbo.Motorista");
            DropTable("dbo.instituicao");
            DropTable("dbo.Local_Instituicao");
            DropTable("dbo.local");
            DropTable("dbo.Local_Passageiro");
            DropTable("dbo.Dia_Semana_Local");
            DropTable("dbo.Dia_Semana");
        }
    }
}
