﻿using Escola4RodasTecnologia.Escola4Rodas.Entity.TypeConfiguration;
using Escola4RodasTecnologia.Escola4Rodas.Dominio;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace Escola4RodasTecnologia.Escola4Rodas.Entity.Context
{
    public class Escola4RodasDbContext : DbContext
    {
        public Escola4RodasDbContext()
        {
            Configuration.LazyLoadingEnabled = false;
            Configuration.ProxyCreationEnabled = false;
        }

        public DbSet<DiaSemana> DiaSemana { get; set; }
        public DbSet<DiaSemanaLocal> DiaSemanaLocal { get; set; }
        public DbSet<DiaSemanaViagem> DiaSemanaViagem { get; set; }
        public DbSet<DocumentoVeiculo> DocumentoVeiculo { get; set; }
        public DbSet<Instituicao> Instituicao { get; set; }
        public DbSet<Local> Local { get; set; }
        public DbSet<LocalPassageiro> LocalPassageiro { get; set; }
        
        public DbSet<LocalInstituicao> LocalInstituicao { get; set; }
        public DbSet<PassageiroInstituicao> PassageiroInstituicao { get; set; }
        
        public DbSet<Motorista> Motorista { get; set; }
        public DbSet<Pagamento> Pagamento { get; set; }
        public DbSet<Passageiro> Passageiro { get; set; }
        public DbSet<Rota> Rota { get; set; }
        //public DbSet<TipoDocumento> TipoDocumento { get; set; }
        public DbSet<Usuario> Usuario { get; set; }
        public DbSet<Veiculo> Veiculo { get; set; }
        public DbSet<Viagem> Viagem { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

            //modelBuilder.Configurations.Add(new DataPagamentoTypeConfiguration());
            modelBuilder.Configurations.Add(new DiaSemanaTypeConfiguration());
            modelBuilder.Configurations.Add(new DiaSemanaLocalTypeConfiguration());
            modelBuilder.Configurations.Add(new DiaSemanaViagemTypeConfiguration());
            modelBuilder.Configurations.Add(new DocumentoVeiculoTypeConfiguration());
            modelBuilder.Configurations.Add(new InstituicaoTypeConfiguration());
            modelBuilder.Configurations.Add(new LocalTypeConfiguration());
            modelBuilder.Configurations.Add(new MotoristaTypeConfiguration());
            modelBuilder.Configurations.Add(new PagamentoTypeConfiguration());
            modelBuilder.Configurations.Add(new PassageiroTypeConfiguration());
            modelBuilder.Configurations.Add(new RotaTypeConfiguration());
            //modelBuilder.Configurations.Add(new TipoDocumentoTypeConfiguration());
            //modelBuilder.Configurations.Add(new TipoLocalTypeConfiguration());
            modelBuilder.Configurations.Add(new UsuarioTypeConfiguration());
            modelBuilder.Configurations.Add(new VeiculoTypeConfiguration());
            modelBuilder.Configurations.Add(new ViagemTypeConfiguration());
            modelBuilder.Configurations.Add(new PassageiroInstituicaoTypeConfiguration());
            modelBuilder.Configurations.Add(new LocalInstituicaoTypeConfiguration());
            modelBuilder.Configurations.Add(new LocalPassageiroTypeConfiguration());

            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();
        }
    }
}
