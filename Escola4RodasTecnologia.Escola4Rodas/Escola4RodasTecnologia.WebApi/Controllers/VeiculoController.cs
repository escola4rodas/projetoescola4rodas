﻿using Escola4RodasTecnologia.Escola4Rodas.Api.DTO;
using Escola4RodasTecnologia.Escola4Rodas.Entity.Context;
using Escola4RodasTecnologia.Escola4Rodas.Repts.Entity;
using Escola4RodasTecnologia.Repositorios.Comum;
using Escola4RodasTecnologia.WebApi.AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Escola4RodasTecnologia.WebApi.Controllers
{
    public class VeiculoController : ApiController
    {

        private IRepositorioGenerico<Escola4Rodas.Dominio.Veiculo, int> _repositorioVeiculo
                = new VeiculoRepositorio(new Escola4RodasDbContext());

        public IHttpActionResult Get()
        {
            List<Escola4Rodas.Dominio.Veiculo> veiculos = _repositorioVeiculo.Selecionar();
            List<VeiculoDTO> dtos = AutoMapperManager.Instance.Mapper.Map<List<Escola4Rodas.Dominio.Veiculo>, List<VeiculoDTO>>(veiculos);
            return Ok(dtos);
        }

        public IHttpActionResult Get(int? id)
        {
            if (!id.HasValue)
            {
                return BadRequest();
            }
            Escola4Rodas.Dominio.Veiculo veiculo = _repositorioVeiculo.SelecionarPorId(id.Value);
            if (veiculo == null)
            {
                return NotFound();
            }

            VeiculoDTO dto = AutoMapperManager.Instance.Mapper.Map<Escola4Rodas.Dominio.Veiculo, VeiculoDTO>(veiculo);

            return Content(HttpStatusCode.OK, dto);
        }

        public IHttpActionResult Post([FromBody]VeiculoDTO dto)
        {
            try
            {
                Escola4Rodas.Dominio.Veiculo veiculo = AutoMapperManager.Instance.Mapper.Map<VeiculoDTO, Escola4Rodas.Dominio.Veiculo>(dto);
                _repositorioVeiculo.Inserir(veiculo);
                return Created($"{Request.RequestUri}/{veiculo.Codigo_Veiculo}", veiculo);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        public IHttpActionResult Put(int? id, [FromBody]VeiculoDTO dto)
        {
            try
            {
                if (!id.HasValue)
                {
                    return BadRequest();
                }
                dto.Codigo_Veiculo = id.Value;
                Escola4Rodas.Dominio.Veiculo veiculo = AutoMapperManager.Instance.Mapper.Map<VeiculoDTO, Escola4Rodas.Dominio.Veiculo>(dto);
                _repositorioVeiculo.Alterar(veiculo);
                return Ok();
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        public IHttpActionResult Delete(int? id)
        {
            try
            {
                if (!id.HasValue)
                {
                    return BadRequest();
                }

                Escola4Rodas.Dominio.Veiculo veiculo = _repositorioVeiculo.SelecionarPorId(id.Value);
                if (veiculo == null)
                {
                    return NotFound();
                }
                _repositorioVeiculo.ExcluirPorId(id.Value);
                return Ok();
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }
    }
}
