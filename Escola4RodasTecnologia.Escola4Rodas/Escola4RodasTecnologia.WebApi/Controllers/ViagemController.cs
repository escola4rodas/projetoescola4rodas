﻿using Escola4RodasTecnologia.Escola4Rodas.Api.DTO;
using Escola4RodasTecnologia.Escola4Rodas.Entity.Context;
using Escola4RodasTecnologia.Escola4Rodas.Repts.Entity;
using Escola4RodasTecnologia.Repositorios.Comum;
using Escola4RodasTecnologia.WebApi.AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Escola4RodasTecnologia.WebApi.Controllers
{
    public class ViagemController : ApiController
    {
        private IRepositorioGenerico<Escola4Rodas.Dominio.Viagem, int> _repositorioViagens
                = new ViagemRepositorio(new Escola4RodasDbContext());

        public IHttpActionResult Get()
        {
            List<Escola4Rodas.Dominio.Viagem> viagens = _repositorioViagens.Selecionar();
            List<ViagemDTO> dtos = AutoMapperManager.Instance.Mapper.Map<List<Escola4Rodas.Dominio.Viagem>, List<ViagemDTO>>(viagens);
            return Ok(dtos);
        }

        public IHttpActionResult Get(int? id)
        {
            if (!id.HasValue)
            {
                return BadRequest();
            }
            Escola4Rodas.Dominio.Viagem viagem = _repositorioViagens.SelecionarPorId(id.Value);
            if (viagem == null)
            {
                return NotFound();
            }

            ViagemDTO dto = AutoMapperManager.Instance.Mapper.Map<Escola4Rodas.Dominio.Viagem, ViagemDTO>(viagem);

            return Content(HttpStatusCode.OK, dto);
        }

        public IHttpActionResult Post([FromBody]ViagemDTO dto)
        {
            try
            {
                Escola4Rodas.Dominio.Viagem viagem = AutoMapperManager.Instance.Mapper.Map<ViagemDTO, Escola4Rodas.Dominio.Viagem>(dto);
                _repositorioViagens.Inserir(viagem);
                return Created($"{Request.RequestUri}/{viagem.Codigo_Veiculo}", viagem);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        public IHttpActionResult Put(int? id, [FromBody]ViagemDTO dto)
        {
            try
            {
                if (!id.HasValue)
                {
                    return BadRequest();
                }
                dto.Codigo_Veiculo = id.Value;
                Escola4Rodas.Dominio.Viagem veiculo = AutoMapperManager.Instance.Mapper.Map<ViagemDTO, Escola4Rodas.Dominio.Viagem>(dto);
                _repositorioViagens.Alterar(veiculo);
                return Ok();
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        public IHttpActionResult Delete(int? id)
        {
            try
            {
                if (!id.HasValue)
                {
                    return BadRequest();
                }

                Escola4Rodas.Dominio.Viagem veiculo = _repositorioViagens.SelecionarPorId(id.Value);
                if (veiculo == null)
                {
                    return NotFound();
                }
                _repositorioViagens.ExcluirPorId(id.Value);
                return Ok();
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }
    }
}
