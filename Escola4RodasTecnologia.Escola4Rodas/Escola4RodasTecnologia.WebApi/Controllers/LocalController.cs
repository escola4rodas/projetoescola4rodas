﻿using Escola4RodasTecnologia.Escola4Rodas.Api.DTO;
using Escola4RodasTecnologia.Escola4Rodas.Entity.Context;
using Escola4RodasTecnologia.Escola4Rodas.Repts.Entity;
using Escola4RodasTecnologia.Repositorios.Comum;
using Escola4RodasTecnologia.WebApi.AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Escola4RodasTecnologia.WebApi.Controllers
{
    public class LocalController : ApiController
    {
        private IRepositorioGenerico<Escola4Rodas.Dominio.Local, int> _repositorioLocal
            = new LocalRepositorio(new Escola4RodasDbContext());

        public IHttpActionResult Get()
        {
            List<Escola4Rodas.Dominio.Local> locais = _repositorioLocal.Selecionar();
            List<LocalDTO> dtos = AutoMapperManager.Instance.Mapper.Map<List<Escola4Rodas.Dominio.Local>, List<LocalDTO>>(locais);
            return Ok(dtos);
        }

        public IHttpActionResult Get(int? id)
        {
            if (!id.HasValue)
            {
                return BadRequest();
            }
            Escola4Rodas.Dominio.Local local = _repositorioLocal.SelecionarPorId(id.Value);
            if (local == null)
            {
                return NotFound();
            }

            LocalDTO dto = AutoMapperManager.Instance.Mapper.Map<Escola4Rodas.Dominio.Local, LocalDTO>(local);

            return Content(HttpStatusCode.OK, dto);
        }

        public IHttpActionResult Post([FromBody]LocalDTO dto)
        {
            try
            {
                Escola4Rodas.Dominio.Local local = AutoMapperManager.Instance.Mapper.Map<LocalDTO, Escola4Rodas.Dominio.Local>(dto);
                _repositorioLocal.Inserir(local);
                return Created($"{Request.RequestUri}/{local.Codigo}", local);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        public IHttpActionResult Put(int? id, [FromBody]LocalDTO dto)
        {
            try
            {
                if (!id.HasValue)
                {
                    return BadRequest();
                }
                dto.Codigo = id.Value;
                Escola4Rodas.Dominio.Local local = AutoMapperManager.Instance.Mapper.Map<LocalDTO, Escola4Rodas.Dominio.Local>(dto);
                _repositorioLocal.Alterar(local);
                return Ok();
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        public IHttpActionResult Delete(int? id)
        {
            try
            {
                if (!id.HasValue)
                {
                    return BadRequest();
                }

                Escola4Rodas.Dominio.Local instituicao = _repositorioLocal.SelecionarPorId(id.Value);
                if (instituicao == null)
                {
                    return NotFound();
                }
                _repositorioLocal.ExcluirPorId(id.Value);
                return Ok();
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }
    }
}
