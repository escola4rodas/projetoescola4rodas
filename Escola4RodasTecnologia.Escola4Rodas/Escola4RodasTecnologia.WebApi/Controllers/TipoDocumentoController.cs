﻿using Escola4RodasTecnologia.Escola4Rodas.Api.DTO;
using Escola4RodasTecnologia.Escola4Rodas.Entity.Context;
using Escola4RodasTecnologia.Escola4Rodas.Repts.Entity;
using Escola4RodasTecnologia.Repositorios.Comum;
using Escola4RodasTecnologia.WebApi.AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Escola4RodasTecnologia.WebApi.Controllers
{
    public class TipoDocumentoController : ApiController
    {
        private IRepositorioGenerico<Escola4Rodas.Dominio.TipoDocumento, int> _repositorioTipoDocumento
            = new TipoDocumentoRepositorio(new Escola4RodasDbContext());

        public IHttpActionResult Get()
        {
            List<Escola4Rodas.Dominio.TipoDocumento> tipoDocumento = _repositorioTipoDocumento.Selecionar();
            List<TipoDocumentoDTO> dtos = AutoMapperManager.Instance.Mapper.Map<List<Escola4Rodas.Dominio.TipoDocumento>, List<TipoDocumentoDTO>>(tipoDocumento);
            return Ok(dtos);
        }

        public IHttpActionResult Get(int? id)
        {
            if (!id.HasValue)
            {
                return BadRequest();
            }
            Escola4Rodas.Dominio.TipoDocumento tipoDocumento = _repositorioTipoDocumento.SelecionarPorId(id.Value);
            if (tipoDocumento == null)
            {
                return NotFound();
            }

            TipoDocumentoDTO dto = AutoMapperManager.Instance.Mapper.Map<Escola4Rodas.Dominio.TipoDocumento, TipoDocumentoDTO>(tipoDocumento);

            return Content(HttpStatusCode.OK, dto);
        }

        public IHttpActionResult Post([FromBody]TipoDocumentoDTO dto)
        {
            try
            {
                Escola4Rodas.Dominio.TipoDocumento tipoDocumento = AutoMapperManager.Instance.Mapper.Map<TipoDocumentoDTO, Escola4Rodas.Dominio.TipoDocumento>(dto);
                _repositorioTipoDocumento.Inserir(tipoDocumento);
                return Created($"{Request.RequestUri}/{tipoDocumento.Codigo}", tipoDocumento);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        public IHttpActionResult Put(int? id, [FromBody]TipoDocumentoDTO dto)
        {
            try
            {
                if (!id.HasValue)
                {
                    return BadRequest();
                }
                dto.Codigo = id.Value;
                Escola4Rodas.Dominio.TipoDocumento rota = AutoMapperManager.Instance.Mapper.Map<TipoDocumentoDTO, Escola4Rodas.Dominio.TipoDocumento>(dto);
                _repositorioTipoDocumento.Alterar(rota);
                return Ok();
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        public IHttpActionResult Delete(int? id)
        {
            try
            {
                if (!id.HasValue)
                {
                    return BadRequest();
                }

                Escola4Rodas.Dominio.TipoDocumento tipoDocumento = _repositorioTipoDocumento.SelecionarPorId(id.Value);
                if (tipoDocumento == null)
                {
                    return NotFound();
                }
                _repositorioTipoDocumento.ExcluirPorId(id.Value);
                return Ok();
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }
    }
}
