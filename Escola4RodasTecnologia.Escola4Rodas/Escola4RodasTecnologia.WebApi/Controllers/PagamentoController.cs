﻿using Escola4RodasTecnologia.Escola4Rodas.Api.DTO;
using Escola4RodasTecnologia.Escola4Rodas.Entity.Context;
using Escola4RodasTecnologia.Escola4Rodas.Repts.Entity;
using Escola4RodasTecnologia.Repositorios.Comum;
using Escola4RodasTecnologia.WebApi.AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Escola4RodasTecnologia.WebApi.Controllers
{
    public class PagamentoController : ApiController
    {
        private IRepositorioGenerico<Escola4Rodas.Dominio.Pagamento, int> _repositorioPagamento
            = new PagamentoRepositorio(new Escola4RodasDbContext());

        public IHttpActionResult Get()
        {
            List<Escola4Rodas.Dominio.Pagamento> pagamento = _repositorioPagamento.Selecionar();
            List<PagamentoDTO> dtos = AutoMapperManager.Instance.Mapper.Map<List<Escola4Rodas.Dominio.Pagamento>, List<PagamentoDTO>>(pagamento);
            return Ok(dtos);
        }

        public IHttpActionResult Get(int? id)
        {
            if (!id.HasValue)
            {
                return BadRequest();
            }
            Escola4Rodas.Dominio.Pagamento pagamento = _repositorioPagamento.SelecionarPorId(id.Value);
            if (pagamento == null)
            {
                return NotFound();
            }

            PagamentoDTO dto = AutoMapperManager.Instance.Mapper.Map<Escola4Rodas.Dominio.Pagamento, PagamentoDTO>(pagamento);

            return Content(HttpStatusCode.OK, dto);
        }

        public IHttpActionResult Post([FromBody]PagamentoDTO dto)
        {

            try
            {
                Escola4Rodas.Dominio.Pagamento pagamento = AutoMapperManager.Instance.Mapper.Map<PagamentoDTO, Escola4Rodas.Dominio.Pagamento>(dto);
                _repositorioPagamento.Inserir(pagamento);
                return Created($"{Request.RequestUri}/{pagamento}", pagamento);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        public IHttpActionResult Put(int? id, [FromBody]PagamentoDTO dto)
        {
            try
            {
                if (!id.HasValue)
                {
                    return BadRequest();
                }
                dto.Codigo = id.Value;
                Escola4Rodas.Dominio.Pagamento pagamento = AutoMapperManager.Instance.Mapper.Map<PagamentoDTO, Escola4Rodas.Dominio.Pagamento>(dto);
                _repositorioPagamento.Alterar(pagamento);
                return Ok();
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        public IHttpActionResult Delete(int? id)
        {
            try
            {
                if (!id.HasValue)
                {
                    return BadRequest();
                }

                Escola4Rodas.Dominio.Pagamento aluno = _repositorioPagamento.SelecionarPorId(id.Value);
                if (aluno == null)
                {
                    return NotFound();
                }
                _repositorioPagamento.ExcluirPorId(id.Value);
                return Ok();
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }
    }
}
