﻿using Escola4RodasTecnologia.Escola4Rodas.Api.DTO;
using Escola4RodasTecnologia.Escola4Rodas.Entity.Context;
using Escola4RodasTecnologia.Escola4Rodas.Repts.Entity;
using Escola4RodasTecnologia.Repositorios.Comum;
using Escola4RodasTecnologia.WebApi.AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Escola4RodasTecnologia.WebApi.Controllers
{
    public class DocumentoVeiculoController : ApiController
    {
        private IRepositorioGenerico<Escola4Rodas.Dominio.DocumentoVeiculo, int> _repositorioDocumentosVeiculo
            = new DocumentoVeiculoRepositorio(new Escola4RodasDbContext());

        public IHttpActionResult Get()
        {
            List<Escola4Rodas.Dominio.DocumentoVeiculo> documentosVeiculo = _repositorioDocumentosVeiculo.Selecionar();
            List<DocumentoVeiculoDTO> dtos = AutoMapperManager.Instance.Mapper.Map<List<Escola4Rodas.Dominio.DocumentoVeiculo>, List<DocumentoVeiculoDTO>>(documentosVeiculo);
            return Ok(dtos);
        }

        public IHttpActionResult Get(int? id)
        {
            if (!id.HasValue)
            {
                return BadRequest();
            }
            Escola4Rodas.Dominio.DocumentoVeiculo documentoVeiculo = _repositorioDocumentosVeiculo.SelecionarPorId(id.Value);
            if (documentoVeiculo == null)
            {
                return NotFound();
            }

            DocumentoVeiculoDTO dto = AutoMapperManager.Instance.Mapper.Map<Escola4Rodas.Dominio.DocumentoVeiculo, DocumentoVeiculoDTO>(documentoVeiculo);

            return Content(HttpStatusCode.OK, dto);
        }

        public IHttpActionResult Post([FromBody]DocumentoVeiculoDTO dto)
        {
            try
            {
                Escola4Rodas.Dominio.DocumentoVeiculo documentoVeiculo = AutoMapperManager.Instance.Mapper.Map<DocumentoVeiculoDTO, Escola4Rodas.Dominio.DocumentoVeiculo>(dto);
                _repositorioDocumentosVeiculo.Inserir(documentoVeiculo);
                return Created($"{Request.RequestUri}/{documentoVeiculo.Codigo}", documentoVeiculo);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        public IHttpActionResult Put(int? id, [FromBody]DocumentoVeiculoDTO dto)
        {
            try
            {
                if (!id.HasValue)
                {
                    return BadRequest();
                }
                dto.Codigo = id.Value;
                Escola4Rodas.Dominio.DocumentoVeiculo documentoVeiculo = AutoMapperManager.Instance.Mapper.Map<DocumentoVeiculoDTO, Escola4Rodas.Dominio.DocumentoVeiculo>(dto);
                _repositorioDocumentosVeiculo.Alterar(documentoVeiculo);
                return Ok();
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        public IHttpActionResult Delete(int? id)
        {
            try
            {
                if (!id.HasValue)
                {
                    return BadRequest();
                }

                Escola4Rodas.Dominio.DocumentoVeiculo documentoVeiculo = _repositorioDocumentosVeiculo.SelecionarPorId(id.Value);
                if (documentoVeiculo == null)
                {
                    return NotFound();
                }
                _repositorioDocumentosVeiculo.ExcluirPorId(id.Value);
                return Ok();
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }
    }
}
