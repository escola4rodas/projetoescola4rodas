﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Escola4RodasTecnologia.Escola4Rodas.Api.DTO
{
    public class InstituicaoDTO
    {
        public int Codigo_Instituicao { get; set; }
        public string Nome { get; set; }
        public int Codigo_Local { get; set; }
        public DateTime Dthr { get; set; }

        public virtual LocalDTO Local { get; set; }
        public virtual List<PassageiroDTO> Passageiros { get; set; }
        public virtual List<MotoristaDTO> Motoristas { get; set; }
        public virtual List<ViagemDTO> Viagens { get; set; }
    }
}