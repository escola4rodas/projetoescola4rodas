﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Escola4RodasTecnologia.Escola4Rodas.Api.DTO
{
    public class LocalDTO
    {
        public int Codigo { get; set; }
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
        public string Nome_Local { get; set; }
        public string Bairro { get; set; }
        public string Nome_Rua { get; set; }
        public int Numero { get; set; }
        public int Codigo_Passageiro { get; set; }
        public virtual PassageiroDTO Passageiro { get; set; }
        public int Codigo_Tipo_Local { get; set; }
        public virtual TipoLocalDTO TipoLocal { get; set; }
        public virtual List<DiaSemanaDTO> DiasSemana { get; set; }
        public DateTime Dthr { get; set; }
    }
}