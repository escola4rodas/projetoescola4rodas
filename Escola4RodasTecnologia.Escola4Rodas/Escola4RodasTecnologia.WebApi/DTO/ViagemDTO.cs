﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Escola4RodasTecnologia.Escola4Rodas.Api.DTO
{
    public class ViagemDTO
    {
        public int Codigo { get; set; }
        public string Nome { get; set; }
        public int Codigo_Veiculo { get; set; }
        public int Codigo_Rota { get; set; }
        public DateTime Data_Inicio { get; set; }
        public DateTime Dthr { get; set; }

        public virtual List<InstituicaoDTO> Instituicoes { get; set; }
        public virtual VeiculoDTO VeiculoViagem { get; set; }
        public virtual RotaDTO RotaViagem { get; set; }
        public virtual List<PassageiroDTO> Passageiros { get; set; }
    }
}