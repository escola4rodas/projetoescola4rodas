﻿using Escola4RodasTecnologia.Escola4Rodas.Dominio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Escola4RodasTecnologia.Escola4Rodas.Api.DTO
{
    public class TipoLocalDTO
    {
        public int Codigo_Tipo_Local { get; set; }
        public string Descricao { get; set; }

        public int Codigo_Local { get; set; }
        public LocalDTO Local { get; set; }
    }
}