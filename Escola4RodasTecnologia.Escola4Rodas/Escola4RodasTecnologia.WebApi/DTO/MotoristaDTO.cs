﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Escola4RodasTecnologia.Escola4Rodas.Api.DTO
{
    public class MotoristaDTO
    {
        public UsuarioDTO Usuario { get; set; }
        public int Codigo_Usuario { get; set; }
        public string Cnh { get; set; }
        public virtual List<ViagemDTO> Viagens { get; set; }
        public virtual List<InstituicaoDTO> Instituicoes { get; set; }
        public virtual List<PassageiroDTO> Passageiros { get; set; }
        public virtual List<VeiculoDTO> Veiculos { get; set; }
    }
}