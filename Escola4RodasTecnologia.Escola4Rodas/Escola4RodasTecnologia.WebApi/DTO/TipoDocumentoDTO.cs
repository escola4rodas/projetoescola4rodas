﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Escola4RodasTecnologia.Escola4Rodas.Api.DTO
{
    public class TipoDocumentoDTO
    {
        public int Codigo { get; set; }
        public string Descricao { get; set; }

        public int Codigo_Veiculo { get; set; }
        public DateTime Dthr { get; set; }

        public virtual DocumentoVeiculoDTO DocumentoVeiculo { get; set; }

    }
}