﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Escola4RodasTecnologia.EscolaApi.DTO
{
    public class MotoristaInstituicaoDTO
    {
        public int Codigo_Instituicao { get; set; }
        public int Codigo_Usuario { get; set; }

        public InstituicaoDTO Instituicao { get; set; }
        public MotoristaDTO Motorista { get; set; }
    }
}