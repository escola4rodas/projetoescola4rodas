﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Escola4RodasTecnologia.EscolaApi.DTO
{
    public class InstituicaoDTO
    {
        public int Codigo_Instituicao { get; set; }
        public string Nome { get; set; }
        public int Codigo_Local { get; set; }
        public DateTime Dthr { get; set; }
        
        public virtual LocalInstituicaoDTO LocalInstituicao { get; set; }
        public virtual List<PassageiroInstituicaoDTO> PassageiroInstituicao { get; set; }
        public virtual List<MotoristaDTO> Motoristas { get; set; }
        public virtual List<ViagemDTO> Viagens { get; set; }
        
    }
}