﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Escola4RodasTecnologia.EscolaApi.DTO
{
    public class DiaSemanaViagemDTO
    {
        public int Codigo { get; set; }
        public DiaSemanaDTO DiaSemana { get; set; }
        public virtual List<ViagemDTO> ViagemDiaSemana { get; set; }
    }
}