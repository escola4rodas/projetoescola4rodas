﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Escola4RodasTecnologia.EscolaApi.DTO
{
    public class DiaSemanaLocalDTO
    {
        public int Codigo { get; set; }
        public DiaSemanaDTO DiaSemana { get; set; }
        public virtual List<LocalPassageiroDTO> DiaSemanaLocais { get; set; }
    }
}