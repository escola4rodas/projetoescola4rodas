﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Escola4RodasTecnologia.EscolaApi.DTO
{
    public class PassageiroViagemDTO
    {
        public int Codigo_Passageiro { get; set; }
        public int Codigo_Viagem { get; set; }
        public PassageiroDTO Passageiro { get; set; }
        public ViagemDTO Viagem { get; set; }
    }
}