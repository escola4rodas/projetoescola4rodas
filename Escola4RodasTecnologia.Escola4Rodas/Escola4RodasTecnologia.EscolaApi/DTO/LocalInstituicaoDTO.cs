﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Escola4RodasTecnologia.EscolaApi.DTO
{
    public class LocalInstituicaoDTO
    {
        public int Codigo_Local { get; set; }
        public LocalDTO Local { get; set; }
        public int Codigo_Instituicao { get; set; }
        public InstituicaoDTO Instituicao { get; set; }
    }
}