﻿using System;
using System.Collections.Generic;

namespace Escola4RodasTecnologia.EscolaApi.DTO
{
    public class PassageiroDTO
    {
        public int Tipo_Viagem { get; set; }
        public int Codigo_Forma_Pagamento { get; set; }
        public int Tipo_Passageiro { get; set; }
        public int Codigo_Usuario { get; set; }
        public DateTime Dthr { get; set; }
        public int Codigo_Motorista { get; set; }

        public virtual MotoristaDTO Motorista { get; set; }
        public virtual UsuarioDTO Usuario { get; set; }
        //public virtual List<InstituicaoDTO> Instituicoes { get; set; }
        public virtual List<PassageiroInstituicaoDTO> PassageiroInstituicao { get; set; }
        public virtual List<PagamentoDTO> Pagamentos { get; set; }
        public virtual List<RotaDTO> Rotas { get; set; }
        public virtual List<LocalPassageiroDTO> LocaisPassageiro { get; set; }
        
        public virtual List<ViagemDTO> Viagens { get; set; }
    }
}