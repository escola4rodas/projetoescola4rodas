﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Escola4RodasTecnologia.EscolaApi.DTO
{
    public class VeiculoDTO
    {
        public int Codigo_Veiculo { get; set; }
        public string Nome { get; set; }
        public string Placa { get; set; }
        public int Carga_Maxima { get; set; }
        public int Codigo_Motorista { get; set; }
        public DateTime Dthr { get; set; }

        public virtual MotoristaDTO Motorista { get; set; }
        public virtual List<DocumentoVeiculoDTO> Documentos { get; set; }
        public virtual List<ViagemDTO> Viagens { get; set; }
    }
}