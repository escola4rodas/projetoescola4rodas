﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Escola4RodasTecnologia.EscolaApi.DTO
{
    public class LocalPassageiroDTO
    {
        public int Codigo_Local { get; set; }
        public LocalDTO Local { get; set; }
        public int Codigo_Tipo_Local { get; set; }
        public int Codigo_Passageiro { get; set; }
        public PassageiroDTO Passageiro { get; set; }
        public virtual List<DiaSemanaLocalDTO> DiasSemanaLocalDTO { get; set; }
    }
}