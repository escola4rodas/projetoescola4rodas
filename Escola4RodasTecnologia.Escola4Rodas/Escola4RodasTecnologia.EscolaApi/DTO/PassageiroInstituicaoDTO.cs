﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Escola4RodasTecnologia.EscolaApi.DTO
{
    public class PassageiroInstituicaoDTO
    {
        public int Codigo_Passageiro { get; set; }
        public int Codigo_Instituicao { get; set; }
        public int Codigo_Tipo_Passageiro { get; set; }

        public PassageiroDTO Passageiro { get; set; }
        public InstituicaoDTO Instituicao { get; set; }
    }
}