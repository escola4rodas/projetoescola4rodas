﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Escola4RodasTecnologia.EscolaApi.DTO
{
    public class UsuarioDTO
    {
        public int Codigo { get; set; }
        public string Nome { get; set; }
        public int Idade { get; set; }
        public string Telefone { get; set; }
        public string Login { get; set; }
        public string Senha { get; set; }
        public string Email { get; set; }
        public DateTime Dthr { get; set; }

        public virtual PassageiroDTO Passageiro { get; set; }
        public virtual MotoristaDTO Motorista { get; set; }
    }
}