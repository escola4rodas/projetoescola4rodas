﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Escola4RodasTecnologia.EscolaApi.DTO
{
    public class RotaDTO
    {
        public int Codigo { get; set; }
        public string Nome { get; set; }
        public virtual List<PassageiroDTO> Passageiros { get; set; }
        public DateTime Dthr { get; set; }
    }
}