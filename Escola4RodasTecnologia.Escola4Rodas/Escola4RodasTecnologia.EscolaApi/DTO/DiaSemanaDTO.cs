﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Escola4RodasTecnologia.EscolaApi.DTO
{
    public class DiaSemanaDTO
    {
        [Required]
        public int Codigo { get; set; }
        [Required]
        public string Dia_Semana { get; set; }
        public virtual List<LocalDTO> Locais { get; set; }

        public virtual DiaSemanaLocalDTO DiaSemanaLocalDto { get; set; }
        public virtual DiaSemanaViagemDTO DiaSemanaViagemDto { get; set; }
    }
}