﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Escola4RodasTecnologia.EscolaApi.DTO
{
    public class PagamentoDTO
    {
        public int Codigo { get; set; }
        public string Descricao { get; set; }
        public DateTime Dthr { get; set; }
        public int Codigo_Passageiro { get; set; }
        public int Mes { get; set; }
        public int Ano { get; set; }
        public int Dia { get; set; }
        public double Valor { get; set; }

        public virtual PassageiroDTO Passageiro { get; set; }
    }
}