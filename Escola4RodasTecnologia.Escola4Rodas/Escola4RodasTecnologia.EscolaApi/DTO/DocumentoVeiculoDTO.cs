﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Escola4RodasTecnologia.EscolaApi.DTO
{
    public class DocumentoVeiculoDTO
    {
        [Required]
        public int Codigo { get; set; }
        public string Nome_Documento { get; set; }
        public string Descricao { get; set; }
        public DateTime Validade { get; set; }
        public DateTime Dthr { get; set; }

        public virtual TipoDocumentoDTO TipoDocumento { get; set; }
        public int Codigo_Tipo_Documento { get; set; }
        public int Codigo_Veiculo { get; set; }
        public virtual VeiculoDTO Veiculo { get; set; }
    }
}