﻿using AutoMapper;
using Escola4RodasTecnologia.EscolaApi.DTO;
using Escola4RodasTecnologia.Escola4Rodas.Dominio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Escola4RodasTecnologia.EscolaApi.AutoMapper
{
    public class AutoMapperManager
    {
        private static readonly Lazy<AutoMapperManager> _instance
           = new Lazy<AutoMapperManager>(() =>
           {
               return new AutoMapperManager();
           });

        public static AutoMapperManager Instance
        {
            get
            {
                return _instance.Value;
            }
        }

        private MapperConfiguration _config;

        public IMapper Mapper
        {
            get
            {
                return _config.CreateMapper();
            }
        }

        private AutoMapperManager()
        {
            _config = new MapperConfiguration((cfg) =>
            {
                cfg.CreateMap<Viagem, ViagemDTO>();
                cfg.CreateMap<ViagemDTO, Viagem>();

                cfg.CreateMap<Veiculo, VeiculoDTO>();
                cfg.CreateMap<VeiculoDTO, Veiculo>();

                cfg.CreateMap<Usuario, UsuarioDTO>();
                cfg.CreateMap<UsuarioDTO, Usuario>();

                cfg.CreateMap<TipoLocal, TipoLocalDTO>();
                cfg.CreateMap<TipoLocalDTO, TipoLocal>();
                
                cfg.CreateMap<TipoDocumento, TipoDocumentoDTO>();
                cfg.CreateMap<TipoDocumentoDTO, TipoDocumento>();

                cfg.CreateMap<PassageiroInstituicao, PassageiroInstituicaoDTO>();
                cfg.CreateMap<PassageiroInstituicaoDTO, PassageiroInstituicao>();

                cfg.CreateMap<Passageiro, PassageiroDTO>();
                cfg.CreateMap<PassageiroDTO, Passageiro>();

                cfg.CreateMap<Pagamento, PagamentoDTO>();
                cfg.CreateMap<PagamentoDTO, Pagamento>();

                cfg.CreateMap<Motorista, MotoristaDTO>();
                cfg.CreateMap<MotoristaDTO, Motorista>();

                cfg.CreateMap<LocalInstituicao, LocalInstituicaoDTO>();
                cfg.CreateMap<LocalInstituicaoDTO, LocalInstituicao>();

                cfg.CreateMap<Local, LocalDTO>();
                cfg.CreateMap<LocalDTO, Local>();

                cfg.CreateMap<LocalPassageiroDTO, LocalPassageiro>();
                cfg.CreateMap<LocalPassageiro, LocalPassageiroDTO>();

                cfg.CreateMap<Instituicao, InstituicaoDTO>();
                cfg.CreateMap<InstituicaoDTO, Instituicao>();

                cfg.CreateMap<DocumentoVeiculo, DocumentoVeiculoDTO>();
                cfg.CreateMap<DocumentoVeiculoDTO, DocumentoVeiculo>();

                cfg.CreateMap<DiaSemanaViagem, DiaSemanaViagemDTO>();
                cfg.CreateMap<DiaSemanaViagemDTO, DiaSemanaViagem>();

                cfg.CreateMap<DiaSemanaLocal, DiaSemanaLocalDTO>();
                cfg.CreateMap<DiaSemanaLocalDTO, DiaSemanaLocal>();

                cfg.CreateMap<DiaSemana, DiaSemanaDTO>();
                cfg.CreateMap<DiaSemanaDTO, DiaSemana>();

                cfg.CreateMap<Rota, RotaDTO>();
                cfg.CreateMap<RotaDTO, Rota>();
            });
        }
    }
}