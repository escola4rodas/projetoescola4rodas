﻿using Escola4RodasTecnologia.Escola4Rodas.Dominio;
using Escola4RodasTecnologia.Escola4Rodas.Entity.Context;
using Escola4RodasTecnologia.Escola4Rodas.Repts.Entity;
using Escola4RodasTecnologia.EscolaApi.AutoMapper;
using Escola4RodasTecnologia.EscolaApi.DTO;
using Escola4RodasTecnologia.Repositorios.Comum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Escola4RodasTecnologia.WebApi.Controllers
{
    //[Authorize]
    public class DiaSemanaController : ApiController
    {
        private IRepositorioGenerico<DiaSemana, int> _repositorioDiasSemana
            = new DiaSemanaRepositorio(new Escola4RodasDbContext());
        //Teste
        public IHttpActionResult Get()
        {
            List<DiaSemana> diasSemana = _repositorioDiasSemana.Selecionar();
            List<DiaSemanaDTO> dtos = AutoMapperManager.Instance.Mapper.Map<List<Escola4Rodas.Dominio.DiaSemana>, List<DiaSemanaDTO>>(diasSemana);
            return Ok(dtos);
        }

        public IHttpActionResult Get(int? id)
        {
            if (!id.HasValue)
            {
                return BadRequest();
            }
            Escola4Rodas.Dominio.DiaSemana diaSemana = _repositorioDiasSemana.SelecionarPorId(id.Value);
            if (diaSemana == null)
            {
                return NotFound();
            }

            DiaSemanaDTO dto = AutoMapperManager.Instance.Mapper.Map<Escola4Rodas.Dominio.DiaSemana, DiaSemanaDTO>(diaSemana);

            return Content(HttpStatusCode.OK, dto);
        }
        
        public IHttpActionResult Post([FromBody]DiaSemanaDTO dto)
        {

            try
            {
                Escola4Rodas.Dominio.DiaSemana diaSemana = AutoMapperManager.Instance.Mapper.Map<DiaSemanaDTO, Escola4Rodas.Dominio.DiaSemana> (dto);
                _repositorioDiasSemana.Inserir(diaSemana);
                return Created($"{Request.RequestUri}/{diaSemana.Codigo}", diaSemana);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        public IHttpActionResult Put(int? id, [FromBody]DiaSemanaDTO dto)
        {
            try
            {
                if (!id.HasValue)
                {
                    return BadRequest();
                }
                dto.Codigo = id.Value;
                Escola4Rodas.Dominio.DiaSemana aluno = AutoMapperManager.Instance.Mapper.Map<DiaSemanaDTO, Escola4Rodas.Dominio.DiaSemana>(dto);
                _repositorioDiasSemana.Alterar(aluno);
                return Ok();
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        public IHttpActionResult Delete(int? id)
        {
            try
            {
                if (!id.HasValue)
                {
                    return BadRequest();
                }

                Escola4Rodas.Dominio.DiaSemana aluno = _repositorioDiasSemana.SelecionarPorId(id.Value);
                if (aluno == null)
                {
                    return NotFound();
                }
                _repositorioDiasSemana.ExcluirPorId(id.Value);
                return Ok();
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }
    }
}
