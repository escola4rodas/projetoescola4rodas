﻿using Escola4RodasTecnologia.Escola4Rodas.Entity.Context;
using Escola4RodasTecnologia.Escola4Rodas.Repts.Entity;
using Escola4RodasTecnologia.EscolaApi.AutoMapper;
using Escola4RodasTecnologia.EscolaApi.DTO;
using Escola4RodasTecnologia.Repositorios.Comum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Escola4RodasTecnologia.WebApi.Controllers
{
    //[Authorize]
    public class RotaController : ApiController
    {
        private IRepositorioGenerico<Escola4Rodas.Dominio.Rota, int> _repositorioRota
            = new RotaRepositorio(new Escola4RodasDbContext());

        public IHttpActionResult Get()
        {
            List<Escola4Rodas.Dominio.Rota> rota = _repositorioRota.Selecionar();
            List<RotaDTO> dtos = AutoMapperManager.Instance.Mapper.Map<List<Escola4Rodas.Dominio.Rota>, List<RotaDTO>>(rota);
            return Ok(dtos);
        }

        public IHttpActionResult Get(int? id)
        {
            if (!id.HasValue)
            {
                return BadRequest();
            }
            Escola4Rodas.Dominio.Rota rota = _repositorioRota.SelecionarPorId(id.Value);
            if (rota == null)
            {
                return NotFound();
            }

            RotaDTO dto = AutoMapperManager.Instance.Mapper.Map<Escola4Rodas.Dominio.Rota, RotaDTO>(rota);

            return Content(HttpStatusCode.OK, dto);
        }

        public IHttpActionResult Post([FromBody]RotaDTO dto)
        {
            try
            {
                Escola4Rodas.Dominio.Rota rota = AutoMapperManager.Instance.Mapper.Map<RotaDTO, Escola4Rodas.Dominio.Rota>(dto);
                _repositorioRota.Inserir(rota);
                return Created($"{Request.RequestUri}/{rota.Codigo}", rota);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        public IHttpActionResult Put(int? id, [FromBody]RotaDTO dto)
        {
            try
            {
                if (!id.HasValue)
                {
                    return BadRequest();
                }
                dto.Codigo = id.Value;
                Escola4Rodas.Dominio.Rota rota = AutoMapperManager.Instance.Mapper.Map<RotaDTO, Escola4Rodas.Dominio.Rota>(dto);
                _repositorioRota.Alterar(rota);
                return Ok();
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        public IHttpActionResult Delete(int? id)
        {
            try
            {
                if (!id.HasValue)
                {
                    return BadRequest();
                }

                Escola4Rodas.Dominio.Rota documentoVeiculo = _repositorioRota.SelecionarPorId(id.Value);
                if (documentoVeiculo == null)
                {
                    return NotFound();
                }
                _repositorioRota.ExcluirPorId(id.Value);
                return Ok();
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }
    }
}
