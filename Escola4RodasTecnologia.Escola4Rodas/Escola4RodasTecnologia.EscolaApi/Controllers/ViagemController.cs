﻿using Escola4RodasTecnologia.Escola4Rodas.Entity.Context;
using Escola4RodasTecnologia.Escola4Rodas.Repts.Entity;
using Escola4RodasTecnologia.EscolaApi.AutoMapper;
using Escola4RodasTecnologia.EscolaApi.DTO;
using Escola4RodasTecnologia.Repositorios.Comum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Escola4RodasTecnologia.WebApi.Controllers
{
   // [Authorize]
    public class ViagemController : ApiController
    {
        private IRepositorioGenerico<Escola4Rodas.Dominio.Viagem, int> _repositorioViagens
                = new ViagemRepositorio(new Escola4RodasDbContext());

        private IRepositorioGenerico<Escola4Rodas.Dominio.DiaSemanaViagem, int> _repositorioDiaSemanaViagem
                = new DiaSemanaViagemRepositorio(new Escola4RodasDbContext());

        private IRepositorioGenerico<Escola4Rodas.Dominio.DiaSemana, int> _repositorioDiaSemana
                = new DiaSemanaRepositorio(new Escola4RodasDbContext());

        private IRepositorioGenerico<Escola4Rodas.Dominio.ViagemInstituicao, int> _repositorioViagemInstituicao
                = new ViagemInstituicaoRepositorio(new Escola4RodasDbContext());

        private IRepositorioGenerico<Escola4Rodas.Dominio.ViagemDiaSemana, int> _viagemDiaSemanaRepositorio
                = new ViagemDiaSemanaRepositorio(new Escola4RodasDbContext());

        private IRepositorioGenerico<Escola4Rodas.Dominio.PassageiroViagem, int> _passageiroViagemRepositorio
                = new PassageiroViagemRepositorio(new Escola4RodasDbContext());

        public IHttpActionResult Get()
        {
            List<Escola4Rodas.Dominio.Viagem> viagens = _repositorioViagens.Selecionar();
            foreach (var item in viagens)
            {
                foreach (var dia in item.DiaSemanaViagem)
                {
                    dia.DiaSemana = _repositorioDiaSemana.SelecionarPorId(dia.Codigo); 
                }
            }
            List<ViagemDTO> dtos = AutoMapperManager.Instance.Mapper.Map<List<Escola4Rodas.Dominio.Viagem>, List<ViagemDTO>>(viagens);
            return Ok(dtos);
        }

        public IHttpActionResult Get(int? id)
        {
            if (!id.HasValue)
            {
                return BadRequest();
            }
            Escola4Rodas.Dominio.Viagem viagem = _repositorioViagens.SelecionarPorId(id.Value);
            if (viagem == null)
            {
                return NotFound();
            }

            ViagemDTO dto = AutoMapperManager.Instance.Mapper.Map<Escola4Rodas.Dominio.Viagem, ViagemDTO>(viagem);

            return Content(HttpStatusCode.OK, dto);
        }

        public IHttpActionResult Post([FromBody]ViagemDTO dto)
        {
            try
            {
                Escola4Rodas.Dominio.Viagem viagem = AutoMapperManager.Instance.Mapper.Map<ViagemDTO, Escola4Rodas.Dominio.Viagem>(dto);
                _repositorioViagens.Inserir(viagem);
                return Created($"{Request.RequestUri}/{viagem.Codigo_Veiculo}", viagem);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        public IHttpActionResult Put([FromBody]ViagemDTO dto)
        {
            try
            {
                if(dto != null)
                {
                    Escola4Rodas.Dominio.Viagem viagem = AutoMapperManager.Instance.Mapper.Map<ViagemDTO, Escola4Rodas.Dominio.Viagem>(dto);
                    //AtualizarViagemInstituicao(viagem);
                    _repositorioViagens.Alterar(viagem);
                    return Ok();
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        private void AdicionarViagemInstituicao(Escola4Rodas.Dominio.Viagem viagem)
        {
            foreach (var item in viagem.Instituicoes)
            {
                Escola4Rodas.Dominio.ViagemInstituicao objViagemInstituicao
                    = new Escola4Rodas.Dominio.ViagemInstituicao
                    {
                        Codigo_Instituicao = item.Codigo_Instituicao,
                        Codigo_Viagem = viagem.Codigo
                    };
                _repositorioViagemInstituicao.Inserir(objViagemInstituicao);
            }
        }

        private void AtualizarViagemInstituicao(Escola4Rodas.Dominio.Viagem viagem)
        {
            List<Escola4Rodas.Dominio.ViagemInstituicao> viagemInstituicaoDisponiveis =
                _repositorioViagemInstituicao.Selecionar()
                    .Where(vi => vi.Codigo_Viagem.Equals(viagem.Codigo))
                    .ToList();
            if(viagemInstituicaoDisponiveis.Count > 0)
            {
                Escola4Rodas.Dominio.Instituicao registro;
                foreach (var item in viagemInstituicaoDisponiveis)
                {
                    registro = viagem.Instituicoes
                        .Find(v => v.Codigo_Instituicao == item.Codigo_Instituicao);
                    if(registro != null)
                    {
                        viagem.Instituicoes.Remove(registro);
                        registro = null;
                    }
                }
                if(viagem.Instituicoes.Count > 0)
                {
                    AdicionarViagemInstituicao(viagem);
                }
            }
        }

        public IHttpActionResult Delete(int? id)
        {
            try
            {
                if (!id.HasValue)
                {
                    return BadRequest();
                }

                Escola4Rodas.Dominio.Viagem veiculo = _repositorioViagens.SelecionarPorId(id.Value);
                if (veiculo == null)
                {
                    return NotFound();
                }
                _repositorioViagens.ExcluirPorId(id.Value);
                return Ok();
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }
    }
}
