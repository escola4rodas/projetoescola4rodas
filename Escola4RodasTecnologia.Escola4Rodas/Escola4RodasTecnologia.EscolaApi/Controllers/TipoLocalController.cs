﻿using Escola4RodasTecnologia.Escola4Rodas.Entity.Context;
using Escola4RodasTecnologia.Escola4Rodas.Repts.Entity;
using Escola4RodasTecnologia.EscolaApi.AutoMapper;
using Escola4RodasTecnologia.EscolaApi.DTO;
using Escola4RodasTecnologia.Repositorios.Comum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Escola4RodasTecnologia.WebApi.Controllers
{
    //[Authorize]
    public class TipoLocalController : ApiController
    {
        private IRepositorioGenerico<Escola4Rodas.Dominio.TipoLocal, int> _repositorioLocal
            = new TipoLocalRepositorio(new Escola4RodasDbContext());

        public IHttpActionResult Get()
        {
            List<Escola4Rodas.Dominio.TipoLocal> tipoLocal = _repositorioLocal.Selecionar();
            List<TipoLocalDTO> dtos = AutoMapperManager.Instance.Mapper.Map<List<Escola4Rodas.Dominio.TipoLocal>, List<TipoLocalDTO>>(tipoLocal);
            return Ok(dtos);
        }

        public IHttpActionResult Get(int? id)
        {
            if (!id.HasValue)
            {
                return BadRequest();
            }
            Escola4Rodas.Dominio.TipoLocal tipoLocal = _repositorioLocal.SelecionarPorId(id.Value);
            if (tipoLocal == null)
            {
                return NotFound();
            }

            TipoLocalDTO dto = AutoMapperManager.Instance.Mapper.Map<Escola4Rodas.Dominio.TipoLocal, TipoLocalDTO>(tipoLocal);

            return Content(HttpStatusCode.OK, dto);
        }

        public IHttpActionResult Post([FromBody]TipoLocalDTO dto)
        {
            try
            {
                Escola4Rodas.Dominio.TipoLocal tipoLocal = AutoMapperManager.Instance.Mapper.Map<TipoLocalDTO, Escola4Rodas.Dominio.TipoLocal>(dto);
                _repositorioLocal.Inserir(tipoLocal);
                return Created($"{Request.RequestUri}/{tipoLocal.Codigo_Local}", tipoLocal);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        public IHttpActionResult Put(int? id, [FromBody]TipoLocalDTO dto)
        {
            try
            {
                if (!id.HasValue)
                {
                    return BadRequest();
                }
                dto.Codigo_Local = id.Value;
                Escola4Rodas.Dominio.TipoLocal rota = AutoMapperManager.Instance.Mapper.Map<TipoLocalDTO, Escola4Rodas.Dominio.TipoLocal>(dto);
                _repositorioLocal.Alterar(rota);
                return Ok();
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        public IHttpActionResult Delete(int? id)
        {
            try
            {
                if (!id.HasValue)
                {
                    return BadRequest();
                }

                Escola4Rodas.Dominio.TipoLocal tipoLocal = _repositorioLocal.SelecionarPorId(id.Value);
                if (tipoLocal == null)
                {
                    return NotFound();
                }
                _repositorioLocal.ExcluirPorId(id.Value);
                return Ok();
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }
    }
}
