﻿using Escola4RodasTecnologia.Escola4Rodas.Entity.Context;
using Escola4RodasTecnologia.Escola4Rodas.Repts.Entity;
using Escola4RodasTecnologia.EscolaApi.AutoMapper;
using Escola4RodasTecnologia.EscolaApi.DTO;
using Escola4RodasTecnologia.Repositorios.Comum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Escola4RodasTecnologia.WebApi.Controllers
{
    //[Authorize]
    public class MotoristaController : ApiController
    {
        private IRepositorioGenerico<Escola4Rodas.Dominio.Motorista, int> _repositoriosMotorista
            = new MotoristaRepositorio(new Escola4RodasDbContext());

        private IRepositorioGenerico<Escola4Rodas.Dominio.Usuario, int> _repositorioUsuario
            = new UsuarioRepositorio(new Escola4RodasDbContext());
        public IHttpActionResult Get()
        {
            List<Escola4Rodas.Dominio.Motorista> motorista = _repositoriosMotorista.Selecionar();
            List<MotoristaDTO> dtos = AutoMapperManager.Instance.Mapper.Map<List<Escola4Rodas.Dominio.Motorista>, List<MotoristaDTO>>(motorista);
            return Ok(dtos);
        }

        public IHttpActionResult Get(int? id)
        {
            if (!id.HasValue)
            {
                return BadRequest();
            }
            Escola4Rodas.Dominio.Motorista motorista = _repositoriosMotorista.SelecionarPorId(id.Value);
            if (motorista == null)
            {
                return NotFound();
            }

            MotoristaDTO dto = AutoMapperManager.Instance.Mapper.Map<Escola4Rodas.Dominio.Motorista, MotoristaDTO>(motorista);

            return Content(HttpStatusCode.OK, dto);
        }

        public IHttpActionResult Post([FromBody]MotoristaDTO dto)
        {

            try
            {
                if(dto != null && dto.Usuario != null)
                {
                    dto.Usuario.Dthr = DateTime.Now;
                    Escola4Rodas.Dominio.Motorista motorista = AutoMapperManager.Instance.Mapper.Map<MotoristaDTO, Escola4Rodas.Dominio.Motorista>(dto);
                    _repositoriosMotorista.Inserir(motorista);
                    return Created($"{Request.RequestUri}/{motorista}", motorista);
                }
                return BadRequest();
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        public IHttpActionResult Put([FromBody]MotoristaDTO dto)
        {
            try
            {
                if (dto != null && dto.Usuario != null)
                {
                    dto.Usuario.Dthr = DateTime.Now;

                    Escola4Rodas.Dominio.Motorista motorista = AutoMapperManager.Instance.Mapper.Map<MotoristaDTO, Escola4Rodas.Dominio.Motorista>(dto);
                    Escola4Rodas.Dominio.Usuario usuario = AutoMapperManager.Instance.Mapper.Map<UsuarioDTO, Escola4Rodas.Dominio.Usuario>(dto.Usuario);

                    _repositorioUsuario.Alterar(usuario);
                    _repositoriosMotorista.Alterar(motorista);

                    return Ok();
                }
                return BadRequest();
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        public IHttpActionResult Delete(int? id)
        {
            try
            {
                if (!id.HasValue)
                {
                    return BadRequest();
                }

                Escola4Rodas.Dominio.Motorista aluno = _repositoriosMotorista.SelecionarPorId(id.Value);
                if (aluno == null)
                {
                    return NotFound();
                }
                _repositoriosMotorista.ExcluirPorId(id.Value);
                return Ok();
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }
    }
}
