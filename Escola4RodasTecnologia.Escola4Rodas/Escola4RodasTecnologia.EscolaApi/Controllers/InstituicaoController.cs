﻿using Escola4RodasTecnologia.Escola4Rodas.Entity.Context;
using Escola4RodasTecnologia.Escola4Rodas.Repts.Entity;
using Escola4RodasTecnologia.EscolaApi.AutoMapper;
using Escola4RodasTecnologia.EscolaApi.DTO;
using Escola4RodasTecnologia.Repositorios.Comum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Escola4RodasTecnologia.WebApi.Controllers
{
    //[Authorize]
    public class InstituicaoController : ApiController
    {
        private IRepositorioGenerico<Escola4Rodas.Dominio.Instituicao, int> _repositorioInstituicoes
            = new InstituicaoRepositorio(new Escola4RodasDbContext());

        private IRepositorioGenerico<Escola4Rodas.Dominio.LocalInstituicao, int> _localInstituicao
             = new LocalInstituicaoRepositorio(new Escola4RodasDbContext());

        public IHttpActionResult Get()
        {
            List<Escola4Rodas.Dominio.Instituicao> instituicoes = _repositorioInstituicoes.Selecionar();
            foreach (var item in instituicoes)
            {
                item.LocalInstituicao = _localInstituicao.Selecionar()
                    .Where(i => i.Instituicao != null 
                        && i.Instituicao.Codigo_Instituicao == item.Codigo_Instituicao)
                    .FirstOrDefault();
            }
            List<InstituicaoDTO> dtos = AutoMapperManager.Instance.Mapper.Map<List<Escola4Rodas.Dominio.Instituicao>, List<InstituicaoDTO>>(instituicoes);
            return Ok(dtos);
        }

        public IHttpActionResult Get(int? id)
        {
            if (!id.HasValue)
            {
                return BadRequest();
            }
            Escola4Rodas.Dominio.Instituicao instituicao = _repositorioInstituicoes.SelecionarPorId(id.Value);
            if (instituicao == null)
            {
                return NotFound();
            }

            InstituicaoDTO dto = AutoMapperManager.Instance.Mapper.Map<Escola4Rodas.Dominio.Instituicao, InstituicaoDTO>(instituicao);

            return Content(HttpStatusCode.OK, dto);
        }

        public IHttpActionResult Post([FromBody]InstituicaoDTO dto)
        {
            try
            {
                Escola4Rodas.Dominio.Instituicao instituicao = AutoMapperManager.Instance.Mapper.Map<InstituicaoDTO, Escola4Rodas.Dominio.Instituicao>(dto);
                _repositorioInstituicoes.Inserir(instituicao);
                return Created($"{Request.RequestUri}/{instituicao.Codigo_Instituicao}", instituicao);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        public IHttpActionResult Put(int? id, [FromBody]InstituicaoDTO dto)
        {
            try
            {
                if (!id.HasValue)
                {
                    return BadRequest();
                }
                dto.Codigo_Instituicao = id.Value;
                Escola4Rodas.Dominio.Instituicao instituicao = AutoMapperManager.Instance.Mapper.Map<InstituicaoDTO, Escola4Rodas.Dominio.Instituicao>(dto);

                _repositorioInstituicoes.Alterar(instituicao);

                return Ok();
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        public IHttpActionResult Delete(int? id)
        {
            try
            {
                if (!id.HasValue)
                {
                    return BadRequest();
                }

                Escola4Rodas.Dominio.Instituicao instituicao = _repositorioInstituicoes.SelecionarPorId(id.Value);
                if (instituicao == null)
                {
                    return NotFound();
                }
                _repositorioInstituicoes.ExcluirPorId(id.Value);
                return Ok();
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }
    }
}
