﻿using Escola4RodasTecnologia.Escola4Rodas.Entity.Context;
using Escola4RodasTecnologia.Escola4Rodas.Repts.Entity;
using Escola4RodasTecnologia.EscolaApi.AutoMapper;
using Escola4RodasTecnologia.EscolaApi.DTO;
using Escola4RodasTecnologia.Repositorios.Comum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Escola4RodasTecnologia.WebApi.Controllers
{
    //[Authorize]
    public class PassageiroController : ApiController
    {
        private IRepositorioGenerico<Escola4Rodas.Dominio.Passageiro, int> _repositorioPassageiro
            = new PassageiroRepositorio(new Escola4RodasDbContext());

        private IRepositorioGenerico<Escola4Rodas.Dominio.Local, int> _repositorioLocal
            = new LocalRepositorio(new Escola4RodasDbContext());

        private IRepositorioGenerico<Escola4Rodas.Dominio.Instituicao, int> _repositorioInstituicao
            = new InstituicaoRepositorio(new Escola4RodasDbContext());

        public IHttpActionResult Get()
        {
            List<Escola4Rodas.Dominio.Passageiro> passageiros = _repositorioPassageiro.Selecionar();

            foreach (var item in passageiros)
            {
                foreach (var localPassageiro in item.LocaisPassageiro)
                {
                    localPassageiro.Local = _repositorioLocal.SelecionarPorId(localPassageiro.Codigo_Local);
                }

                foreach (var passageiroInstituicao in item.PassageiroInstituicao)
                {
                    passageiroInstituicao.Instituicao = _repositorioInstituicao.Selecionar().Where(i => i.Codigo_Instituicao == passageiroInstituicao.Codigo_Instituicao).FirstOrDefault();
                }
            }

            List<PassageiroDTO> dtos = AutoMapperManager.Instance.Mapper.Map<List<Escola4Rodas.Dominio.Passageiro>, List<PassageiroDTO>>(passageiros);
            return Ok(dtos);
        }

        public IHttpActionResult Get(int? id)
        {
            if (!id.HasValue)
            {
                return BadRequest();
            }
            Escola4Rodas.Dominio.Passageiro passageiro = _repositorioPassageiro.SelecionarPorId(id.Value);
            if (passageiro == null)
            {
                return NotFound();
            }

            PassageiroDTO dto = AutoMapperManager.Instance.Mapper.Map<Escola4Rodas.Dominio.Passageiro, PassageiroDTO>(passageiro);

            return Content(HttpStatusCode.OK, dto);
        }

        public IHttpActionResult Post([FromBody]PassageiroDTO dto)
        {
            try
            {
                Escola4Rodas.Dominio.Passageiro passageiro = AutoMapperManager.Instance.Mapper.Map<PassageiroDTO, Escola4Rodas.Dominio.Passageiro>(dto);
                _repositorioPassageiro.Inserir(passageiro);
                return Created($"{Request.RequestUri}/{passageiro.Codigo_Usuario}", passageiro);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        public IHttpActionResult Put([FromBody]PassageiroDTO dto)
        {
            try
            {
                if(dto != null)
                {   
                    Escola4Rodas.Dominio.Passageiro passageiro = AutoMapperManager.Instance.Mapper.Map<PassageiroDTO, Escola4Rodas.Dominio.Passageiro>(dto);
                    _repositorioPassageiro.Alterar(passageiro);
                }
                else
                {
                    return BadRequest();
                }

                return Ok();
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        public IHttpActionResult Delete(int? id)
        {
            try
            {
                if (!id.HasValue)
                {
                    return BadRequest();
                }

                Escola4Rodas.Dominio.Passageiro documentoVeiculo = _repositorioPassageiro.SelecionarPorId(id.Value);
                if (documentoVeiculo == null)
                {
                    return NotFound();
                }
                _repositorioPassageiro.ExcluirPorId(id.Value);
                return Ok();
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

    }
}
