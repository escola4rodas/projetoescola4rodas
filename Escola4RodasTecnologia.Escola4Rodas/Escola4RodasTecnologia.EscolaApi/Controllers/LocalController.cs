﻿using Escola4RodasTecnologia.Escola4Rodas.Entity.Context;
using Escola4RodasTecnologia.Escola4Rodas.Repts.Entity;
using Escola4RodasTecnologia.EscolaApi.AutoMapper;
using Escola4RodasTecnologia.EscolaApi.DTO;
using Escola4RodasTecnologia.Repositorios.Comum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Escola4RodasTecnologia.WebApi.Controllers
{
    //[Authorize]
    public class LocalController : ApiController
    {
        private IRepositorioGenerico<Escola4Rodas.Dominio.Local, int> _repositorioLocal
            = new LocalRepositorio(new Escola4RodasDbContext());

        private IRepositorioGenerico<Escola4Rodas.Dominio.LocalPassageiro, int> _repositorioLocalPassageiro
            = new LocalPassageiroRepositorio(new Escola4RodasDbContext());

        private IRepositorioGenerico<Escola4Rodas.Dominio.LocalInstituicao, int> _repositorioLocalInstituicao
            = new LocalInstituicaoRepositorio(new Escola4RodasDbContext());

        public IHttpActionResult Get()
        {
            List<Escola4Rodas.Dominio.Local> locais = _repositorioLocal.Selecionar();
            List<LocalDTO> dtos = AutoMapperManager.Instance.Mapper.Map<List<Escola4Rodas.Dominio.Local>, List<LocalDTO>>(locais);
            return Ok(dtos);
        }

        public IHttpActionResult Get(int? id)
        {
            if (!id.HasValue)
            {
                return BadRequest();
            }
            Escola4Rodas.Dominio.Local local = _repositorioLocal.SelecionarPorId(id.Value);
            if (local == null)
            {
                return NotFound();
            }

            LocalDTO dto = AutoMapperManager.Instance.Mapper.Map<Escola4Rodas.Dominio.Local, LocalDTO>(local);

            return Content(HttpStatusCode.OK, dto);
        }

        public IHttpActionResult Post([FromBody]LocalDTO dto)
        {
            try
            {
                Escola4Rodas.Dominio.Local local = AutoMapperManager.Instance.Mapper.Map<LocalDTO, Escola4Rodas.Dominio.Local>(dto);
                _repositorioLocal.Inserir(local);
                return Created($"{Request.RequestUri}/{local.Codigo}", local);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        public IHttpActionResult Put(int? id, [FromBody]LocalDTO dto)
        {
            try
            {
                if (!id.HasValue)
                {
                    return BadRequest();
                }
                dto.Codigo = id.Value;
                Escola4Rodas.Dominio.Local local = AutoMapperManager.Instance.Mapper.Map<LocalDTO, Escola4Rodas.Dominio.Local>(dto);
                _repositorioLocal.Alterar(local);
                return Ok();
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        public IHttpActionResult Delete(int? id)
        {
            try
            {
                if (!id.HasValue)
                {
                    return BadRequest();
                }

                Escola4Rodas.Dominio.Local instituicao = _repositorioLocal.SelecionarPorId(id.Value);
                if (instituicao == null)
                {
                    return NotFound();
                }
                _repositorioLocal.ExcluirPorId(id.Value);
                return Ok();
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        //Locais Passageiro
        public IHttpActionResult BuscarLocaisPorPassageiro(int? codigoPassageiro)
        {
            if (!codigoPassageiro.HasValue)
            {
                return BadRequest();
            }
            List<Escola4Rodas.Dominio.LocalPassageiro> locaisPassageiro = 
                _repositorioLocalPassageiro.Selecionar()
                .Where(lp => lp.Codigo_Passageiro ==  codigoPassageiro)
                .ToList();

            if (locaisPassageiro == null || locaisPassageiro.Count == 0)
            {
                return NotFound();
            }

            List<LocalPassageiroDTO> dto = 
                AutoMapperManager.Instance.Mapper.Map<List<Escola4Rodas.Dominio.LocalPassageiro>, List<LocalPassageiroDTO>>(locaisPassageiro);

            return Content(HttpStatusCode.OK, dto);
        }

        public IHttpActionResult InserirLocalPassageiro([FromBody]LocalPassageiroDTO dto)
        {
            try
            {
                Escola4Rodas.Dominio.LocalPassageiro localPassageiro = AutoMapperManager.Instance.Mapper.Map<LocalPassageiroDTO, Escola4Rodas.Dominio.LocalPassageiro>(dto);
                _repositorioLocalPassageiro.Inserir(localPassageiro);
                return Created($"{Request.RequestUri}/{localPassageiro.Codigo_Local}", localPassageiro);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        public IHttpActionResult AtualizarLocalPassageiro([FromBody]LocalPassageiroDTO dto)
        {
            try
            {
                if (dto == null)
                {
                    return BadRequest();
                }
                
                Escola4Rodas.Dominio.LocalPassageiro localPassageiro = AutoMapperManager.Instance.Mapper.Map<LocalPassageiroDTO, Escola4Rodas.Dominio.LocalPassageiro>(dto);
                _repositorioLocalPassageiro.Alterar(localPassageiro);
                return Ok();
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        public IHttpActionResult ExcluirLocalPassageiro(int? codigoLocalPassageiro)
        {
            try
            {
                if (!codigoLocalPassageiro.HasValue)
                {
                    return BadRequest();
                }

                Escola4Rodas.Dominio.LocalPassageiro localPassageiro = 
                    _repositorioLocalPassageiro.SelecionarPorId(codigoLocalPassageiro.Value);

                if (localPassageiro == null)
                {
                    return NotFound();
                }
                _repositorioLocalPassageiro.ExcluirPorId(codigoLocalPassageiro.Value);
                return Ok();
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }
        //Locais Instituição
        public IHttpActionResult InserirLocalInstituicao([FromBody]LocalInstituicaoDTO dto)
        {
            try
            {
                Escola4Rodas.Dominio.LocalInstituicao localInstituicao = AutoMapperManager.Instance.Mapper.Map<LocalInstituicaoDTO, Escola4Rodas.Dominio.LocalInstituicao>(dto);
                _repositorioLocalInstituicao.Inserir(localInstituicao);
                return Created($"{Request.RequestUri}/{localInstituicao.Codigo_Local}", localInstituicao);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        public IHttpActionResult AtualizarLocalInstituicao([FromBody]LocalInstituicaoDTO dto)
        {
            try
            {
                if (dto == null)
                {
                    return BadRequest();
                }

                Escola4Rodas.Dominio.LocalInstituicao localInstituicao = AutoMapperManager.Instance.Mapper.Map<LocalInstituicaoDTO, Escola4Rodas.Dominio.LocalInstituicao>(dto);
                _repositorioLocalInstituicao.Alterar(localInstituicao);
                return Ok();
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        public IHttpActionResult ExcluirLocalInstituicao(int? codigoLocalInstituicao)
        {
            try
            {
                if (!codigoLocalInstituicao.HasValue)
                {
                    return BadRequest();
                }

                Escola4Rodas.Dominio.LocalInstituicao localInstituicao =
                    _repositorioLocalInstituicao.SelecionarPorId(codigoLocalInstituicao.Value);

                if (localInstituicao == null)
                {
                    return NotFound();
                }
                _repositorioLocalPassageiro.ExcluirPorId(codigoLocalInstituicao.Value);
                return Ok();
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        public IHttpActionResult BuscarLocalInstituicaoPorCodigo(int? codigoInstituicao)
        {
            if (!codigoInstituicao.HasValue)
            {
                return BadRequest();
            }
            Escola4Rodas.Dominio.LocalInstituicao localInstituicao = _repositorioLocalInstituicao.SelecionarPorId(codigoInstituicao.Value);
            if (localInstituicao == null)
            {
                return NotFound();
            }

            LocalInstituicaoDTO dto = AutoMapperManager.Instance.Mapper.Map<Escola4Rodas.Dominio.LocalInstituicao, LocalInstituicaoDTO>(localInstituicao);

            return Content(HttpStatusCode.OK, dto);
        }

    }
}
