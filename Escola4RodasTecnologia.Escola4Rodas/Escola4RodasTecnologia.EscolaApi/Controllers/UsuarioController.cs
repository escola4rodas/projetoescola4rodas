﻿using Escola4RodasTecnologia.Escola4Rodas.Entity.Context;
using Escola4RodasTecnologia.Escola4Rodas.Repts.Entity;
using Escola4RodasTecnologia.EscolaApi.AutoMapper;
using Escola4RodasTecnologia.EscolaApi.DTO;
using Escola4RodasTecnologia.Repositorios.Comum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Escola4RodasTecnologia.EscolaApi.Controllers
{
    public class UsuarioController : ApiController
    {
        private IRepositorioGenerico<Escola4Rodas.Dominio.Motorista, int> _repositoriosMotorista
            = new MotoristaRepositorio(new Escola4RodasDbContext());

        private IRepositorioGenerico<Escola4Rodas.Dominio.Passageiro, int> _repositoriosPassageiro
            = new PassageiroRepositorio(new Escola4RodasDbContext());

        private IRepositorioGenerico<Escola4Rodas.Dominio.Usuario, int> _repositoriosUsuario
             = new UsuarioRepositorio(new Escola4RodasDbContext());

        public IHttpActionResult Login([FromBody]UsuarioDTO dto)
        {
            try
            {
                if (dto != null)
                {
                    Escola4Rodas.Dominio.Usuario usuarioLogado = null;
                    usuarioLogado = _repositoriosUsuario.Selecionar()
                        .Where(u => (u.Login.Equals(dto.Login) || u.Email.Equals(dto.Email)) && u.Senha.Equals(dto.Senha))
                        .FirstOrDefault();


                    if(usuarioLogado != null )
                    {
                        UsuarioDTO respostaUsuarioLogadoDto = AutoMapperManager.Instance.Mapper.Map<Escola4Rodas.Dominio.Usuario, UsuarioDTO>(usuarioLogado);
                        return Content(HttpStatusCode.OK, respostaUsuarioLogadoDto);
                    }
                    else
                    {
                        return BadRequest();
                    }
                }
                return BadRequest();
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }
    }
}
