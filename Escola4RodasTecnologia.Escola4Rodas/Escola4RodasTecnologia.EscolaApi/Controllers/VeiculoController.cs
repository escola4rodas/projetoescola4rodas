﻿using Escola4RodasTecnologia.Escola4Rodas.Entity.Context;
using Escola4RodasTecnologia.Escola4Rodas.Repts.Entity;
using Escola4RodasTecnologia.EscolaApi.AutoMapper;
using Escola4RodasTecnologia.EscolaApi.DTO;
using Escola4RodasTecnologia.Repositorios.Comum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Escola4RodasTecnologia.WebApi.Controllers
{
    //[Authorize]
    [RoutePrefix("api/veiculo")]
    public class VeiculoController : ApiController
    {

        
        private IRepositorioGenerico<Escola4Rodas.Dominio.Veiculo, int> _repositorioVeiculo
                = new VeiculoRepositorio(new Escola4RodasDbContext());

        private IRepositorioGenerico<Escola4Rodas.Dominio.Motorista, int> _repositorioMotorista
                = new MotoristaRepositorio(new Escola4RodasDbContext());

        private IRepositorioGenerico<Escola4Rodas.Dominio.DocumentoVeiculo, int> _repositorioDocumento
                = new DocumentoVeiculoRepositorio(new Escola4RodasDbContext());

        public IHttpActionResult Get()
        {
            List<Escola4Rodas.Dominio.Veiculo> veiculos = _repositorioVeiculo.Selecionar();
            List<VeiculoDTO> dtos = AutoMapperManager.Instance.Mapper.Map<List<Escola4Rodas.Dominio.Veiculo>, List<VeiculoDTO>>(veiculos);
            return Ok(dtos);
        }

        public IHttpActionResult Get(int? id)
        {
            if (!id.HasValue)
            {
                return BadRequest();
            }
            Escola4Rodas.Dominio.Veiculo veiculo = _repositorioVeiculo.SelecionarPorId(id.Value);
            if (veiculo == null)
            {
                return NotFound();
            }

            VeiculoDTO dto = AutoMapperManager.Instance.Mapper.Map<Escola4Rodas.Dominio.Veiculo, VeiculoDTO>(veiculo);

            return Content(HttpStatusCode.OK, dto);
        }

        [HttpPost]
        public IHttpActionResult Post([FromBody]VeiculoDTO dto)
        {
            try
            {
                Escola4Rodas.Dominio.Veiculo veiculo = AutoMapperManager.Instance.Mapper.Map<VeiculoDTO, Escola4Rodas.Dominio.Veiculo>(dto);
                veiculo.Dthr = DateTime.Now;
                _repositorioVeiculo.Inserir(veiculo);
                return Created($"{Request.RequestUri}/{veiculo.Codigo_Veiculo}", veiculo);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpPut]
        public IHttpActionResult Put([FromBody]VeiculoDTO dto)
        {
            try
            {

                if (dto == null)
                {
                    return BadRequest();
                }
                else
                {
                    
                    _repositorioMotorista.SelecionarPorId(dto.Motorista.Codigo_Usuario);
                    Escola4Rodas.Dominio.Veiculo veiculo = AutoMapperManager.Instance.Mapper.Map<VeiculoDTO, Escola4Rodas.Dominio.Veiculo>(dto);
                    _repositorioVeiculo.Alterar(veiculo);
                    return Ok();
                }
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpDelete]
        public IHttpActionResult Delete(int? id)
        {
            
            try
            {
                if (!id.HasValue)
                {
                    return BadRequest();
                }

                Escola4Rodas.Dominio.Veiculo veiculo = _repositorioVeiculo.SelecionarPorId(id.Value);
                if (veiculo == null)
                {
                    return NotFound();
                }
                else
                {
                    foreach (var item in veiculo.Documentos)
                    {
                        _repositorioDocumento.Excluir(item);
                    }
                    _repositorioVeiculo.ExcluirPorId(id.Value);
                }
                
                return Ok();
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }
    }
}
