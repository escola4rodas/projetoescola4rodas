﻿using Escola4RodasTecnologia.Escola4Rodas.Dominio;
using Escola4RodasTecnologia.Escola4Rodas.Entity.Context;
using Escola4RodasTecnologia.Escola4Rodas.Repts.Entity;
using Escola4RodasTecnologia.EscolaApi.DTO;
using Escola4RodasTecnologia.Repositorios.Comum;
using Microsoft.Owin.Security.OAuth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;

namespace Escola4RodasTecnologia.EscolaApi.Providers
{
    public class SimpleAuthServerProvider : OAuthAuthorizationServerProvider
    {
        private IRepositorioGenerico<Escola4Rodas.Dominio.Motorista, int> _repositoriosMotorista
            = new MotoristaRepositorio(new Escola4RodasDbContext());

        private IRepositorioGenerico<Escola4Rodas.Dominio.Passageiro, int> _repositoriosPassageiro
            = new PassageiroRepositorio(new Escola4RodasDbContext());

        private IRepositorioGenerico<Escola4Rodas.Dominio.Usuario, int> _repositoriosUsuario
             = new UsuarioRepositorio(new Escola4RodasDbContext());

        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            //context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new string[] { "*" });
            string[] origem = { "http://localhost:8100" };//context.Request.Headers.GetValues("Host");
            context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", origem);


            context.Validated();
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            Usuario usuarioLogado = _repositoriosUsuario.Selecionar()
                .Where(m => (m.Login.Equals(context.UserName) || m.Email.Equals(context.UserName)) && m.Senha.Equals(context.Password))
                .FirstOrDefault();
            if(usuarioLogado != null)
            {
                if (context.OwinContext.Request.Headers["Tipo_Usuario"].Equals("motorista"))
                {
                    Motorista motoristaLogado = _repositoriosMotorista.SelecionarPorId(usuarioLogado.Codigo);
                    context.OwinContext.Authentication.SignIn();
                    string[] dadosMotorista = { motoristaLogado.Codigo_Usuario.ToString() };
                    context.OwinContext.Response.Headers.Add("Codigo_Usuario", dadosMotorista);
                    
                }
                else if (context.OwinContext.Request.Headers["Tipo_Usuario"].Equals("passageiro"))
                {
                    Passageiro passageiroLogado = _repositoriosPassageiro.SelecionarPorId(usuarioLogado.Codigo);
                    context.OwinContext.Authentication.SignIn();
                    string[] dadosUsuario = { passageiroLogado.Codigo_Usuario.ToString() };
                    context.OwinContext.Response.Headers.Add("Codigo_Usuario", dadosUsuario);
                }
            }
            else
            {
                context.SetError("invalid_user_or_password", "Usuário e/ou senha incorretos.");
                return;
            }

            ClaimsIdentity identity = new ClaimsIdentity(context.Options.AuthenticationType);
            context.Validated(identity);
        }

        public override Task MatchEndpoint(OAuthMatchEndpointContext context)
        {

            if (context.OwinContext.Request.Method == "OPTIONS" && context.IsTokenEndpoint)
            {
                context.OwinContext.Response.Headers.Add("Access-Control-Allow-Methods", new string[] { "POST", "GET", "OPTIONS", "PUT", "DELETE" });
                context.OwinContext.Response.Headers.Add("Access-Control-Allow-Headers", new string[] { "Codigo_Usuario", "Tipo_Usuario", "Content-Type", "Grant-type" });
                context.OwinContext.Response.Headers.Add("Access-Control-Max-Age", new string[] { "9000" });
                context.OwinContext.Response.Headers.Add("Access-Control-Allow-Credentials", new string[] { "true" });

                string[] origem = { "http://localhost:8100" };//context.Request.Headers.GetValues("Host");
                context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", origem);

                context.RequestCompleted();

                return Task.FromResult<object>(null);
            }

            return base.MatchEndpoint(context);
        }
    }
}