﻿using System.Web;
using System.Web.Mvc;

namespace Escola4RodasTecnologia.EscolaApi
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
