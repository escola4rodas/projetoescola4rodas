﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Escola4RodasTecnologia.Escola4Rodas.Dominio
{
    public class LocalPassageiro
    {
        public int Codigo_Local { get; set; }
        public Local Local { get; set; }
        public int Codigo_Tipo_Local { get; set; }
        public int Codigo_Passageiro { get; set; }
        public Passageiro Passageiro { get; set; }
        public virtual List<DiaSemanaLocal> DiasSemanaLocal { get; set; }
        //public List<LocalPassageiroDiaSemana> LocalPassageiroDiaSemana { get; set; }
    }
}
