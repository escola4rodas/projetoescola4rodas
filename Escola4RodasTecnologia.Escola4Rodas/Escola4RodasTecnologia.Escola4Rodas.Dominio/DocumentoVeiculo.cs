﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Escola4RodasTecnologia.Escola4Rodas.Dominio
{
    public class DocumentoVeiculo
    {
        public int Codigo { get; set; }
        public string Nome_Documento { get; set; }
        public string Descricao { get; set; }
        public DateTime Validade { get; set; }
        public DateTime Dthr { get; set; }
        
        public int Codigo_Tipo_Documento { get; set; }
        public int Codigo_Veiculo { get; set; }
        public virtual Veiculo Veiculo { get; set; }

    }
}
