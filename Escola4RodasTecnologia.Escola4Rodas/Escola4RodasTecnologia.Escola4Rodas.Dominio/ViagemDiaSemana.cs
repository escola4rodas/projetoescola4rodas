﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Escola4RodasTecnologia.Escola4Rodas.Dominio
{
    public class ViagemDiaSemana
    {
        public int Codigo_Viagem { get; set; }
        public int Codigo_Dia_Semana { get; set; }

        public DiaSemanaViagem DiaSemana{ get; set; }
        public Viagem Viagem { get; set; }
    }
}
