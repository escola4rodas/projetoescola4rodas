﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Escola4RodasTecnologia.Escola4Rodas.Dominio
{
    public class PassageiroViagem
    {
        public int Codigo_Passageiro { get; set; }
        public int Codigo_Viagem { get; set; }
        public Passageiro Passageiro { get; set; }
        public Viagem Viagem { get; set; }
    }
}
