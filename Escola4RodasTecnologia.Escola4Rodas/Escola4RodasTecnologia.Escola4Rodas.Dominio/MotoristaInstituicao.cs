﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Escola4RodasTecnologia.Escola4Rodas.Dominio
{
    public class MotoristaInstituicao
    {
        public int Codigo_Instituicao { get; set; }
        public int Codigo_Usuario{ get; set; }

        public Instituicao Instituicao { get; set; }
        public Motorista Motorista { get; set; }
    }
}
