﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Escola4RodasTecnologia.Escola4Rodas.Dominio
{
    public class DiaSemanaLocal
    {
        public int Codigo { get; set; }
        public DiaSemana DiaSemana { get; set; }
        public virtual List<LocalPassageiro> DiaSemanaLocais { get; set; }
    }
}
