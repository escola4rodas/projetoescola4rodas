﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Escola4RodasTecnologia.Escola4Rodas.Dominio
{
    public class PassageiroInstituicao
    {
        public int Codigo_Passageiro { get; set; }
        public int Codigo_Instituicao { get; set; }
        public int Codigo_Tipo_Passageiro { get; set; }

        public Passageiro Passageiro { get; set; }
        public Instituicao Instituicao { get; set; }
    }
}
