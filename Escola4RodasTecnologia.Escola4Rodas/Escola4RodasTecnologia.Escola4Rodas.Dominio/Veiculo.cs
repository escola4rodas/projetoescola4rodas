﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Escola4RodasTecnologia.Escola4Rodas.Dominio
{
    public class Veiculo
    {
        public int Codigo_Veiculo { get; set; }
        public string Nome { get; set; }
        public string Placa { get; set; }
        public int Carga_Maxima { get; set; }
        public int Codigo_Motorista { get; set; }
        public DateTime Dthr { get; set; }

        public virtual Motorista Motorista { get; set; }
        public virtual List<DocumentoVeiculo> Documentos { get; set; }
        public virtual List<Viagem> Viagens { get; set; }
    }
}
