﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Escola4RodasTecnologia.Escola4Rodas.Dominio
{
    public class Motorista
    {
        public Usuario Usuario { get; set; }
        public int Codigo_Usuario { get; set; }
        public string Cnh { get; set; }
        public virtual List<Viagem> Viagens { get; set; }
        public virtual List<Instituicao> Instituicoes { get; set; }
        public virtual List<Passageiro> Passageiros { get; set; }
        public virtual List<Veiculo> Veiculos { get; set; }
    }
}
