﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Escola4RodasTecnologia.Escola4Rodas.Dominio
{
    public class ViagemInstituicao
    {
        public int Codigo_Viagem { get; set; }
        public int Codigo_Instituicao { get; set; }
        
        public Viagem Viagem { get; set; }
        public Instituicao Instituicao { get; set; }
    }
}
