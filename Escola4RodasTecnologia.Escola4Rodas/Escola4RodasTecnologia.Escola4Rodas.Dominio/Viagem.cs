﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Escola4RodasTecnologia.Escola4Rodas.Dominio
{
    public class Viagem
    {
        public int Codigo { get; set; }
        public string Nome { get; set; }
        public int Codigo_Veiculo { get; set; }
        //public int Codigo_Rota { get; set; }
        public DateTime Data_Inicio { get; set; }
        public DateTime Dthr { get; set; }
       // public virtual Rota RotaViagem { get; set; }
        public int Tipo_Viagem { get; set; }
        public int Codigo_Dia_Semana { get; set; }
        public virtual List<Instituicao> Instituicoes { get; set; }
        public virtual Veiculo VeiculoViagem { get; set; }
        public virtual List<Passageiro> Passageiros { get; set; }

        public List<DiaSemanaViagem> DiaSemanaViagem { get; set; }
        //public List<ViagemDiaSemana> ViagemDiaSemana { get; set; }
        //public virtual List<ViagemInstituicao> ViagemInstituicao { get; set; }
    }
}
