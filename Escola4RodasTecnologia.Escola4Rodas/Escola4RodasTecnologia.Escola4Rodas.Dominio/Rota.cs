﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Escola4RodasTecnologia.Escola4Rodas.Dominio
{
    public class Rota
    {
        public int Codigo { get; set; }
        public string Nome {get; set;}
        public virtual List<Passageiro> Passageiros { get; set; }
        public DateTime Dthr { get; set; }
    }
}
