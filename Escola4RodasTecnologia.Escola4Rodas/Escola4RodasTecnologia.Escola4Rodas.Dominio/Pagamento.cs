﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Escola4RodasTecnologia.Escola4Rodas.Dominio
{
    public class Pagamento
    {
        public int Codigo { get; set; }
        public string Descricao { get; set; }
        public DateTime Dthr { get; set; }
        public int Codigo_Passageiro { get; set; }
        public int Mes { get; set; }
        public int Ano { get; set; }
        public int Dia { get; set; }
        public double Valor { get; set; }

        public virtual Passageiro Passageiro { get; set; }
    }
}
