﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Escola4RodasTecnologia.Escola4Rodas.Dominio
{
    public class LocalPassageiroDiaSemana
    {
        public int Codigo_Local { get; set; }
        public int Codigo_Dia_Semana { get; set; }

        public LocalPassageiro LocalPassageiro { get; set; }
        public DiaSemanaLocal DiaSemanaLocal { get; set; }
    }
}
