﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Escola4RodasTecnologia.Escola4Rodas.Dominio
{
    public class Local
    {
        public int Codigo { get; set; }
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
        public string Nome_Local { get; set; }
        public string Bairro { get; set; }
        public string Nome_Rua { get; set; }
        public int Numero { get; set; }
        public DateTime Dthr { get; set; }

        public LocalPassageiro LocalPassageiro { get; set; }
        public LocalInstituicao LocalInstituicao { get; set; }
    }
}
