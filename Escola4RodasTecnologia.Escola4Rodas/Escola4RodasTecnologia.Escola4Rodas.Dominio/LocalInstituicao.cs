﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Escola4RodasTecnologia.Escola4Rodas.Dominio
{
    public class LocalInstituicao
    {
        
        public int Codigo_Local { get; set; }
        public Local Local { get; set; }
        public int Codigo_Instituicao { get; set; }
        public Instituicao Instituicao { get; set; }
    }
}
