﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Escola4RodasTecnologia.Escola4Rodas.Dominio
{
    public class Passageiro
    {
        public int Tipo_Viagem { get; set; }
        public int Codigo_Forma_Pagamento { get; set; }
        public int Tipo_Passageiro { get; set; }
        public int Codigo_Usuario { get; set; }
        public DateTime Dthr { get; set; }
        public int Codigo_Motorista { get; set; }

        public virtual Motorista Motorista { get; set; }
        public virtual Usuario Usuario { get; set; }
        //public virtual List<Instituicao> Instituicoes { get; set; }
        public virtual List<PassageiroInstituicao> PassageiroInstituicao { get; set; }
        public virtual List<Pagamento> Pagamentos { get; set; }
        public virtual List<Rota> Rotas { get; set; }
        public virtual List<LocalPassageiro> LocaisPassageiro { get; set; }
        public virtual List<Viagem> Viagens { get; set; }
        
    }
}
