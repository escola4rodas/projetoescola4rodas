﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Escola4RodasTecnologia.Escola4Rodas.Dominio
{
    public class DiaSemana
    {
        public int Codigo { get; set; }
        public string Dia_Semana { get; set; }
        public virtual DiaSemanaLocal DiaSemanaLocal { get; set; }
        public virtual DiaSemanaViagem DiaSemanaViagem { get; set; }
        //public virtual List<LocalPassageiro> Locais { get; set; }    
    }

}
