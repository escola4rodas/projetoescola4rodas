﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Escola4RodasTecnologia.Escola4Rodas.Dominio
{
    public class Instituicao
    {
        public int Codigo_Instituicao { get; set; }
        public string Nome { get; set; }
        public int Codigo_Local { get; set; }
        public DateTime Dthr { get; set; }

        public int Codigo_Local_Instituicao { get; set; }
        public virtual LocalInstituicao LocalInstituicao { get; set; }
        //public virtual List<Passageiro> Passageiros { get; set; }
        public virtual List<PassageiroInstituicao> PassageiroInstituicao { get; set; }
        public virtual List<Motorista> Motoristas { get; set; }
        public virtual List<Viagem> Viagens { get; set; }
    }
}
