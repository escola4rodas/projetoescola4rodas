﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Escola4RodasTecnologia.Escola4Rodas.Dominio
{
    public class TipoLocal
    {
        public int Codigo_Tipo_Local { get; set; }
        public string Descricao { get; set; }

        public int Codigo_Local { get; set; }
        public Local Local { get; set; }
    }
}
