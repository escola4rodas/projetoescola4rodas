﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Escola4RodasTecnologia.Comum.Entity
{
    public abstract class Escola4RodasEntityAbstractConfig<TEntidade> : EntityTypeConfiguration<TEntidade>
        where TEntidade : class
    {
        public Escola4RodasEntityAbstractConfig()
        {
            ConfigurarNomeTabela();
            ConfigurarCamposTabela();
            ConfigurarChavePrimariaTabela();
            ConfigurarChaveEstrangeiraTabela();
        }

        protected abstract void ConfigurarChaveEstrangeiraTabela();
        protected abstract void ConfigurarChavePrimariaTabela();
        protected abstract void ConfigurarCamposTabela();
        protected abstract void ConfigurarNomeTabela();

    }
}
