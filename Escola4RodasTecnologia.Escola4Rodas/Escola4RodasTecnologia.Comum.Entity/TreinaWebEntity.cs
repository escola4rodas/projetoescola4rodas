﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Escola4RodasTecnologia.Comum.Entity
{
    public abstract class TreinaWebEntity<TEntidade> : EntityTypeConfiguration<TEntidade>
        where TEntidade : class
    {
        public TreinaWebEntity()
        {
            ConfigurarNomeTabela();
            ConfigurarCampostabela();
            ConfigurarChavePrimariaTabela();
            ConfigurarChaveEstrangeiraTabela();
        }

        protected abstract void ConfigurarChaveEstrangeiraTabela();
        protected abstract void ConfigurarChavePrimariaTabela();
        protected abstract void ConfigurarCampostabela();
        protected abstract void ConfigurarNomeTabela();
    }
}
